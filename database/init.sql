-- MySQL dump 10.13  Distrib 5.7.30-33, for Linux (x86_64)
--
-- Host: mysql45.unoeuro.com    Database: korfitz_com_db2
-- ------------------------------------------------------
-- Server version	5.7.30-33-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50717 SELECT COUNT(*) INTO @rocksdb_has_p_s_session_variables FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'performance_schema' AND TABLE_NAME = 'session_variables' */;
/*!50717 SET @rocksdb_get_is_supported = IF (@rocksdb_has_p_s_session_variables, 'SELECT COUNT(*) INTO @rocksdb_is_supported FROM performance_schema.session_variables WHERE VARIABLE_NAME=\'rocksdb_bulk_load\'', 'SELECT 0') */;
/*!50717 PREPARE s FROM @rocksdb_get_is_supported */;
/*!50717 EXECUTE s */;
/*!50717 DEALLOCATE PREPARE s */;
/*!50717 SET @rocksdb_enable_bulk_load = IF (@rocksdb_is_supported, 'SET SESSION rocksdb_bulk_load = 1', 'SET @rocksdb_dummy_bulk_load = 0') */;
/*!50717 PREPARE s FROM @rocksdb_enable_bulk_load */;
/*!50717 EXECUTE s */;
/*!50717 DEALLOCATE PREPARE s */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `street` varchar(255) DEFAULT NULL,
                             `street_2` varchar(255) DEFAULT NULL,
                             `zip_code` varchar(255) DEFAULT NULL,
                             `city` varchar(255) DEFAULT NULL,
                             `country` varchar(255) DEFAULT NULL,
                             `country_code` varchar(255) DEFAULT NULL,
                             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                             `updated_at` timestamp NULL DEFAULT NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `addresses_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (1,'Fælledvej 15, st. th.',NULL,'4000','Roskilde','Denmark','DK','2020-07-09 19:30:15',NULL),(2,'Hindebregnen 11',NULL,'4070','Kr. Hyllinge','Denmark','DK','2020-07-09 19:46:03',NULL);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `analytics`
--

DROP TABLE IF EXISTS `analytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analytics` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `name` varchar(255) DEFAULT NULL,
                             `version` varchar(255) DEFAULT NULL,
                             `ua` varchar(255) DEFAULT NULL COMMENT 'User-agent',
                             `service` varchar(255) DEFAULT NULL,
                             `page` varchar(255) DEFAULT NULL,
                             `geo_ip` varchar(255) DEFAULT NULL,
                             `geo_latitude` varchar(255) DEFAULT NULL,
                             `geo_longitude` varchar(255) DEFAULT NULL,
                             `geo_postal` varchar(255) DEFAULT NULL,
                             `geo_city` varchar(255) DEFAULT NULL,
                             `geo_country_code` varchar(255) DEFAULT NULL,
                             `geo_country` varchar(255) DEFAULT NULL,
                             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                             `updated_at` timestamp NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `browser_detections_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=795 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `analytics`
--

LOCK TABLES `analytics` WRITE;
/*!40000 ALTER TABLE `analytics` DISABLE KEYS */;
/*!40000 ALTER TABLE `analytics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appstore`
--

DROP TABLE IF EXISTS `appstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appstore` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `manufacturer` int(11) DEFAULT NULL,
                            `title` varchar(255) DEFAULT NULL COMMENT 'This field is used internally only... All public texts should come from the "meta" property',
                            `meta` int(11) DEFAULT NULL,
                            `version` varchar(255) DEFAULT NULL,
                            `architecture` varchar(255) DEFAULT NULL,
                            `file_type` varchar(255) DEFAULT NULL,
                            `download_path` varchar(255) DEFAULT NULL,
                            `requires_license` tinyint(1) DEFAULT NULL,
                            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                            `updated_at` timestamp NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `app_store_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appstore`
--

LOCK TABLES `appstore` WRITE;
/*!40000 ALTER TABLE `appstore` DISABLE KEYS */;
INSERT INTO `appstore` VALUES (1,1,'Windows 10',NULL,'1909','x86','iso','cfcfgnx',1,'2020-07-21 16:41:00',NULL),(2,1,'Windows 10',NULL,'1909','x64','iso','trydxf g',1,'2020-07-21 20:16:18',NULL),(3,1,'Ubuntu 18.04',NULL,'LTS','x64','iso',NULL,0,'2020-07-22 12:05:23',NULL),(4,1,'Windows 7',NULL,'Service Pack 1','x64','iso',NULL,1,'2020-07-22 12:05:23',NULL);
/*!40000 ALTER TABLE `appstore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appstore_meta`
--

DROP TABLE IF EXISTS `appstore_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appstore_meta` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `appstore_id` int(11) DEFAULT NULL,
                                 `title` varchar(255) DEFAULT NULL,
                                 `description` varchar(255) DEFAULT NULL,
                                 `description_extended` text,
                                 `specifications` int(11) DEFAULT NULL,
                                 `locale` varchar(255) DEFAULT NULL,
                                 `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                 `updated_at` timestamp NULL,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `appstore_meta_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appstore_meta`
--

LOCK TABLES `appstore_meta` WRITE;
/*!40000 ALTER TABLE `appstore_meta` DISABLE KEYS */;
INSERT INTO `appstore_meta` VALUES (1,1,'Windows 10','mhvhmvhjmv','mjvmjhv',1,'dk','2020-07-21 16:41:50',NULL),(2,2,'Windows 10','tndn','fnfnf',1,'dk','2020-07-22 12:04:44',NULL),(3,3,'Ubuntu 18.04 LTS',NULL,NULL,NULL,NULL,'2020-07-22 12:04:44',NULL),(4,4,'Windows 7',NULL,NULL,NULL,NULL,'2020-07-22 12:04:44',NULL);
/*!40000 ALTER TABLE `appstore_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appstore_meta_specs`
--

DROP TABLE IF EXISTS `appstore_meta_specs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appstore_meta_specs` (
                                       `id` int(11) NOT NULL AUTO_INCREMENT,
                                       `processor` varchar(255) DEFAULT NULL,
                                       `memory` varchar(255) DEFAULT NULL,
                                       `available_space` varchar(255) DEFAULT NULL,
                                       `graphics` varchar(255) DEFAULT NULL,
                                       `display` varchar(255) DEFAULT NULL,
                                       `internet` varchar(255) DEFAULT NULL,
                                       `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                       `updated_at` timestamp NULL,
                                       PRIMARY KEY (`id`),
                                       UNIQUE KEY `appstore_meta_specs_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appstore_meta_specs`
--

LOCK TABLES `appstore_meta_specs` WRITE;
/*!40000 ALTER TABLE `appstore_meta_specs` DISABLE KEYS */;
INSERT INTO `appstore_meta_specs` VALUES (1,'1,9 mhz','4gb','25gb','HD','600x800','1','2020-07-21 16:42:43',NULL);
/*!40000 ALTER TABLE `appstore_meta_specs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `belongings`
--

DROP TABLE IF EXISTS `belongings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `belongings` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `type` varchar(255) DEFAULT NULL,
                              `uid` int(11) DEFAULT NULL,
                              `meta` int(11) DEFAULT NULL,
                              `purchase` int(11) DEFAULT NULL,
                              `gallery` int(11) DEFAULT NULL,
                              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                              `updated_at` timestamp NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `belongings_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `belongings`
--

LOCK TABLES `belongings` WRITE;
/*!40000 ALTER TABLE `belongings` DISABLE KEYS */;
/*!40000 ALTER TABLE `belongings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `belongings_images`
--

DROP TABLE IF EXISTS `belongings_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `belongings_images` (
                                     `id` int(11) NOT NULL AUTO_INCREMENT,
                                     `belonging_id` int(11) DEFAULT NULL,
                                     `thumbnail` tinyint(1) DEFAULT NULL,
                                     `path` varchar(255) DEFAULT NULL,
                                     `description` varchar(255) DEFAULT NULL,
                                     `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                     `updated_at` timestamp NULL,
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `bel_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `belongings_images`
--

LOCK TABLES `belongings_images` WRITE;
/*!40000 ALTER TABLE `belongings_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `belongings_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `belongings_meta`
--

DROP TABLE IF EXISTS `belongings_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `belongings_meta` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `belonging_id` int(11) DEFAULT NULL,
                                   `title` varchar(255) DEFAULT NULL,
                                   `description` varchar(255) DEFAULT NULL,
                                   `locale` varchar(255) DEFAULT NULL,
                                   `created_at` timestamp NULL DEFAULT NULL,
                                   `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `belongings_meta_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `belongings_meta`
--

LOCK TABLES `belongings_meta` WRITE;
/*!40000 ALTER TABLE `belongings_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `belongings_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `belongings_purchases`
--

DROP TABLE IF EXISTS `belongings_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `belongings_purchases` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `belonging_id` int(11) DEFAULT NULL,
                                        `receipt` varchar(255) DEFAULT NULL,
                                        `order_number` varchar(255) DEFAULT NULL,
                                        `date` date DEFAULT NULL,
                                        `store` varchar(255) DEFAULT NULL,
                                        `address` int(11) DEFAULT NULL,
                                        `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                        `updated_at` timestamp NULL,
                                        PRIMARY KEY (`id`),
                                        UNIQUE KEY `belongings_purchases_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `belongings_purchases`
--

LOCK TABLES `belongings_purchases` WRITE;
/*!40000 ALTER TABLE `belongings_purchases` DISABLE KEYS */;
/*!40000 ALTER TABLE `belongings_purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `belongings_uid`
--

DROP TABLE IF EXISTS `belongings_uid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `belongings_uid` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `belonging_id` int(11) DEFAULT NULL,
                                  `serial_number` varchar(255) DEFAULT NULL,
                                  `product_number` varchar(255) DEFAULT NULL,
                                  `imei` varchar(255) DEFAULT NULL,
                                  `vin` varchar(255) DEFAULT NULL,
                                  `color` varchar(255) DEFAULT NULL,
                                  `size` varchar(255) DEFAULT NULL,
                                  `special_marks` varchar(255) DEFAULT NULL,
                                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                  `updated_at` timestamp NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `belongings_uid_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `belongings_uid`
--

LOCK TABLES `belongings_uid` WRITE;
/*!40000 ALTER TABLE `belongings_uid` DISABLE KEYS */;
/*!40000 ALTER TABLE `belongings_uid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `garage` int(11) DEFAULT NULL,
                        `license` varchar(255) DEFAULT NULL,
                        `make` int(11) DEFAULT NULL,
                        `model` varchar(255) DEFAULT NULL,
                        `variant` varchar(255) DEFAULT NULL,
                        `service` int(11) DEFAULT NULL,
                        `kilometer` int(11) DEFAULT NULL,
                        `data_card` int(11) DEFAULT NULL,
                        `modifications` int(11) DEFAULT NULL,
                        `purchased` int(11) DEFAULT NULL,
                        `sold` int(11) DEFAULT NULL,
                        `insurance` int(11) DEFAULT NULL,
                        `registration` int(11) DEFAULT NULL,
                        `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                        `updated_at` timestamp NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `garage_cars_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES (1,1,'CE36433',1,'Touran','Highline 2,0TDI DSG',NULL,345000,1,NULL,NULL,NULL,1,1,'2020-07-09 11:09:39',NULL);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_datacard`
--

DROP TABLE IF EXISTS `cars_datacard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_datacard` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `model_code` varchar(255) DEFAULT NULL,
                                 `model_year` int(11) DEFAULT NULL,
                                 `long_life` tinyint(1) DEFAULT NULL,
                                 `kilowatt` int(11) DEFAULT NULL,
                                 `horse_power` int(11) DEFAULT NULL,
                                 `engine_name` varchar(255) DEFAULT NULL,
                                 `engine_code` varchar(255) DEFAULT NULL,
                                 `engine_number` varchar(255) DEFAULT NULL,
                                 `transmission_name` varchar(255) DEFAULT NULL,
                                 `transmission_code` varchar(255) DEFAULT NULL,
                                 `color_name` varchar(255) DEFAULT NULL,
                                 `color_code` varchar(255) DEFAULT NULL,
                                 `color_number` varchar(255) DEFAULT NULL,
                                 `wheel_lock_code` varchar(255) DEFAULT NULL,
                                 `radio_code` varchar(255) DEFAULT NULL,
                                 `key_cut_code` varchar(255) DEFAULT NULL,
                                 `immo_pin_code` varchar(255) DEFAULT NULL,
                                 `alarm_code` varchar(255) DEFAULT NULL,
                                 `options` int(11) DEFAULT NULL,
                                 `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                 `updated_at` timestamp NULL,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `cars_datacard_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_datacard`
--

LOCK TABLES `cars_datacard` WRITE;
/*!40000 ALTER TABLE `cars_datacard` DISABLE KEYS */;
INSERT INTO `cars_datacard` VALUES (1,'1T1H43',2006,1,103,140,'2,0 TDI','BKD','A74511','DSG6','HXT','Deep Black Pearl','2T2TJM','LC9X',NULL,NULL,'3273',NULL,NULL,NULL,'2020-07-09 11:16:43',NULL);
/*!40000 ALTER TABLE `cars_datacard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_datacard_options`
--

DROP TABLE IF EXISTS `cars_datacard_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_datacard_options` (
                                         `id` int(11) NOT NULL AUTO_INCREMENT,
                                         `datacard_id` int(11) DEFAULT NULL,
                                         `name` varchar(255) DEFAULT NULL,
                                         `contains` varchar(255) DEFAULT NULL,
                                         `price` int(11) DEFAULT NULL,
                                         `code` varchar(255) DEFAULT NULL,
                                         `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                         `updated_at` timestamp NULL,
                                         PRIMARY KEY (`id`),
                                         UNIQUE KEY `cars_datacard_options_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_datacard_options`
--

LOCK TABLES `cars_datacard_options` WRITE;
/*!40000 ALTER TABLE `cars_datacard_options` DISABLE KEYS */;
INSERT INTO `cars_datacard_options` VALUES (1,1,'Metallak','Deep Black Pearl',10000,'pa','2020-07-09 17:23:19',NULL),(2,1,'DK pakke','Oliefyr',15000,'dkp','2020-07-09 17:24:22',NULL);
/*!40000 ALTER TABLE `cars_datacard_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_insurances`
--

DROP TABLE IF EXISTS `cars_insurances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_insurances` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `company` varchar(255) DEFAULT NULL,
                                   `policy` int(11) DEFAULT NULL,
                                   `renewal_date` date DEFAULT NULL,
                                   `type` varchar(255) DEFAULT NULL,
                                   `annual_price` int(11) DEFAULT NULL,
                                   `payment_form` varchar(255) DEFAULT NULL,
                                   `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                   `updated_at` timestamp NULL,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `cars_insurances_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_insurances`
--

LOCK TABLES `cars_insurances` WRITE;
/*!40000 ALTER TABLE `cars_insurances` DISABLE KEYS */;
INSERT INTO `cars_insurances` VALUES (1,'Nykredit Forsikring',9859200,'2021-05-01','Kasko',6076,'Månedlig betaling med Betalingsservice','2020-07-09 18:18:45',NULL);
/*!40000 ALTER TABLE `cars_insurances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_manufacturers`
--

DROP TABLE IF EXISTS `cars_manufacturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_manufacturers` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `company` int(11) DEFAULT NULL,
                                      `group` varchar(255) DEFAULT NULL,
                                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                      `updated_at` timestamp NULL,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `cars_manufacturers_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_manufacturers`
--

LOCK TABLES `cars_manufacturers` WRITE;
/*!40000 ALTER TABLE `cars_manufacturers` DISABLE KEYS */;
INSERT INTO `cars_manufacturers` VALUES (1,1,'VAG','2020-07-11 11:27:30',NULL),(2,1,'VAG','2020-07-11 11:27:30',NULL),(3,1,'VAG','2020-07-11 11:27:30',NULL),(4,1,'VAG','2020-07-11 11:27:31',NULL);
/*!40000 ALTER TABLE `cars_manufacturers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_manufacturers_models`
--

DROP TABLE IF EXISTS `cars_manufacturers_models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_manufacturers_models` (
                                             `id` int(11) NOT NULL AUTO_INCREMENT,
                                             `manufacturer_id` int(11) DEFAULT NULL,
                                             `name` varchar(255) DEFAULT NULL,
                                             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                             `updated_at` timestamp NULL,
                                             PRIMARY KEY (`id`),
                                             UNIQUE KEY `cars_manufacturers_models_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_manufacturers_models`
--

LOCK TABLES `cars_manufacturers_models` WRITE;
/*!40000 ALTER TABLE `cars_manufacturers_models` DISABLE KEYS */;
INSERT INTO `cars_manufacturers_models` VALUES (1,1,'A1','2020-07-11 11:27:50',NULL);
/*!40000 ALTER TABLE `cars_manufacturers_models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_modifications`
--

DROP TABLE IF EXISTS `cars_modifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_modifications` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `car_id` int(11) DEFAULT NULL,
                                      `modification_id` int(11) DEFAULT NULL,
                                      `price` int(11) DEFAULT NULL,
                                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                      `updated_at` timestamp NULL,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `cars_modifications_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_modifications`
--

LOCK TABLES `cars_modifications` WRITE;
/*!40000 ALTER TABLE `cars_modifications` DISABLE KEYS */;
INSERT INTO `cars_modifications` VALUES (6,1,1,1000,'2020-07-11 09:31:27',NULL);
/*!40000 ALTER TABLE `cars_modifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_purchases`
--

DROP TABLE IF EXISTS `cars_purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_purchases` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `car_id` int(11) DEFAULT NULL,
                                  `date` date DEFAULT NULL,
                                  `price` int(11) DEFAULT NULL,
                                  `from` int(11) DEFAULT NULL,
                                  `note` text,
                                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                  `updated_at` timestamp NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `cars_purchase_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_purchases`
--

LOCK TABLES `cars_purchases` WRITE;
/*!40000 ALTER TABLE `cars_purchases` DISABLE KEYS */;
INSERT INTO `cars_purchases` VALUES (1,1,'2012-07-07',34900,NULL,NULL,'2020-07-09 11:31:00',NULL);
/*!40000 ALTER TABLE `cars_purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_registrations`
--

DROP TABLE IF EXISTS `cars_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_registrations` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `description` varchar(255) DEFAULT NULL,
                                      `vin_number` varchar(255) DEFAULT NULL,
                                      `reg_number` varchar(255) DEFAULT NULL,
                                      `reg_status` tinyint(1) DEFAULT NULL,
                                      `first_reg_date` date DEFAULT NULL,
                                      `latest_mot` date DEFAULT NULL,
                                      `latest_cancel_date` date DEFAULT NULL,
                                      `fuel_type` varchar(255) DEFAULT NULL,
                                      `fuel_consumption` varchar(255) DEFAULT NULL,
                                      `max_power` varchar(255) DEFAULT NULL,
                                      `cubic_capacity` varchar(255) DEFAULT NULL,
                                      `cylinders` int(11) DEFAULT NULL,
                                      `own_weight` int(11) DEFAULT NULL,
                                      `total_weight` int(11) DEFAULT NULL,
                                      `drive_weight` int(11) DEFAULT NULL,
                                      `axle_load` int(11) DEFAULT NULL,
                                      `axle_amount` int(11) DEFAULT NULL,
                                      `towbar_installed` tinyint(1) DEFAULT NULL,
                                      `pull_with_brakes` int(11) DEFAULT NULL,
                                      `pull_without_brakes` int(11) DEFAULT NULL,
                                      `emission_class` int(11) DEFAULT NULL COMMENT 'Euro norm',
                                      `emission_sticker` varchar(255) DEFAULT NULL COMMENT 'E.g 4(green). It''s mainly used in germany...',
                                      `approval_number` varchar(255) DEFAULT NULL COMMENT 'Known as "Type godkendelse" in denmark',
                                      `type_plate_location` varchar(255) DEFAULT NULL,
                                      `annual_tax` varchar(255) DEFAULT NULL COMMENT 'Årlig grøn ejerafgift',
                                      `energy_class` varchar(255) DEFAULT NULL,
                                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                      `updated_at` timestamp NULL,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `cars_registrations_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_registrations`
--

LOCK TABLES `cars_registrations` WRITE;
/*!40000 ALTER TABLE `cars_registrations` DISABLE KEYS */;
INSERT INTO `cars_registrations` VALUES (1,'VOLKSWAGEN TOURAN VAN 2,0 TDI AUT.','WVGZZZ1TZ6W242924','CE36433',1,'2006-10-30','2019-04-16',NULL,'Diesel','15,3 km/l','103 kw','1968 ccm',4,1625,2300,750,1200,2,1,1500,750,5,NULL,'A13364','Bund bag højre sæde','7.220 kr.','D','2020-07-09 16:25:46',NULL);
/*!40000 ALTER TABLE `cars_registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_sales`
--

DROP TABLE IF EXISTS `cars_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_sales` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `car_id` int(11) DEFAULT NULL,
                              `date` date DEFAULT NULL,
                              `price` int(11) DEFAULT NULL,
                              `to` int(11) DEFAULT NULL,
                              `note` text,
                              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                              `updated_at` timestamp NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `cars_sales_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_sales`
--

LOCK TABLES `cars_sales` WRITE;
/*!40000 ALTER TABLE `cars_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `cars_sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `icon` varchar(255) DEFAULT NULL,
                              `meta` int(11) DEFAULT NULL,
                              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                              `updated_at` timestamp NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `vault_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'fa fa-tools',NULL,'2020-07-30 18:01:41','2020-07-30 18:01:41'),(2,'fa fa-dollar',NULL,'2020-07-30 18:01:41','2020-07-30 18:01:41');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_meta`
--

DROP TABLE IF EXISTS `categories_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_meta` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `category_id` int(11) DEFAULT NULL,
                                   `title` varchar(255) DEFAULT NULL,
                                   `description` varchar(255) DEFAULT NULL,
                                   `locale` varchar(255) DEFAULT NULL,
                                   `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                   `updated_at` timestamp NULL,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `vault_categories_meta_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_meta`
--

LOCK TABLES `categories_meta` WRITE;
/*!40000 ALTER TABLE `categories_meta` DISABLE KEYS */;
INSERT INTO `categories_meta` VALUES (1,1,'DFB','dzbdb','dk','2020-07-30 18:02:21',NULL),(2,1,'DAFB FM','dfbdafb','gb','2020-07-30 18:02:21',NULL),(3,2,'FNNX','dsfbadsb','dk','2020-07-30 18:02:21',NULL),(4,2,'DFNGSZX','sfbSB','gb','2020-07-30 18:02:21',NULL);
/*!40000 ALTER TABLE `categories_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
                             `id` int(11) NOT NULL AUTO_INCREMENT,
                             `logo` varchar(255) DEFAULT NULL,
                             `name` varchar(255) DEFAULT NULL,
                             `branches` int(11) DEFAULT NULL,
                             `contacts` int(11) DEFAULT NULL,
                             `vat` varchar(255) DEFAULT NULL,
                             `manager` int(11) DEFAULT NULL,
                             `industry` int(11) DEFAULT NULL,
                             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                             `updated_at` timestamp NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `workshop_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,NULL,'Bosch Carservice',NULL,NULL,NULL,1,1,'2020-07-09 19:50:32',NULL);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies_branches`
--

DROP TABLE IF EXISTS `companies_branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies_branches` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `company_id` int(11) DEFAULT NULL,
                                      `logo` varchar(255) DEFAULT NULL,
                                      `name` varchar(255) DEFAULT NULL,
                                      `address` int(11) DEFAULT NULL,
                                      `departments` int(11) DEFAULT NULL,
                                      `p_number` varchar(255) DEFAULT NULL,
                                      `manager` int(11) DEFAULT NULL,
                                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                      `updated_at` timestamp NULL,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `companies_branches_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies_branches`
--

LOCK TABLES `companies_branches` WRITE;
/*!40000 ALTER TABLE `companies_branches` DISABLE KEYS */;
INSERT INTO `companies_branches` VALUES (1,1,NULL,'Tune',1,NULL,NULL,1,'2020-07-21 10:15:39',NULL),(2,1,NULL,'Gadstrup',2,NULL,NULL,1,'2020-07-21 10:15:39',NULL);
/*!40000 ALTER TABLE `companies_branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies_branches_departments`
--

DROP TABLE IF EXISTS `companies_branches_departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies_branches_departments` (
                                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `branch_id` int(11) DEFAULT NULL,
                                                  `title` varchar(255) DEFAULT NULL,
                                                  `telephone` varchar(255) DEFAULT NULL,
                                                  `fax` varchar(255) DEFAULT NULL,
                                                  `email` varchar(255) DEFAULT NULL,
                                                  `manager` int(11) DEFAULT NULL,
                                                  `opening_hours` int(11) DEFAULT NULL,
                                                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                                  `updated_at` timestamp NULL,
                                                  PRIMARY KEY (`id`),
                                                  UNIQUE KEY `companies_branches_departments_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies_branches_departments`
--

LOCK TABLES `companies_branches_departments` WRITE;
/*!40000 ALTER TABLE `companies_branches_departments` DISABLE KEYS */;
INSERT INTO `companies_branches_departments` VALUES (1,1,'Salgsafdelingen','ddtnsrtnrs','dbzdb','dfgxnf',1,NULL,'2020-07-21 11:40:09',NULL);
/*!40000 ALTER TABLE `companies_branches_departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies_branches_departments_opening`
--

DROP TABLE IF EXISTS `companies_branches_departments_opening`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies_branches_departments_opening` (
                                                          `id` int(11) NOT NULL AUTO_INCREMENT,
                                                          `department_id` int(11) DEFAULT NULL,
                                                          `day_of_week` int(11) DEFAULT NULL,
                                                          `opening_hour` int(11) DEFAULT NULL,
                                                          `opening_minute` int(11) DEFAULT NULL,
                                                          `closing_hour` int(11) DEFAULT NULL,
                                                          `closing_minute` int(11) DEFAULT NULL,
                                                          `holiday_closed` tinyint(1) DEFAULT NULL,
                                                          `primary_opening_hours` tinyint(1) DEFAULT NULL,
                                                          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                                          `updated_at` timestamp NULL,
                                                          PRIMARY KEY (`id`),
                                                          UNIQUE KEY `companies_branches_opening_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies_branches_departments_opening`
--

LOCK TABLES `companies_branches_departments_opening` WRITE;
/*!40000 ALTER TABLE `companies_branches_departments_opening` DISABLE KEYS */;
INSERT INTO `companies_branches_departments_opening` VALUES (1,1,0,7,30,16,0,1,NULL,'2020-07-21 10:28:45',NULL),(2,1,1,7,30,16,0,1,NULL,'2020-07-21 10:28:45',NULL),(3,1,2,7,30,16,0,1,NULL,'2020-07-21 10:28:45',NULL),(4,1,3,7,30,16,0,1,NULL,'2020-07-21 10:28:45',NULL),(5,1,4,7,30,14,0,1,NULL,'2020-07-21 10:28:45',NULL);
/*!40000 ALTER TABLE `companies_branches_departments_opening` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies_industries`
--

DROP TABLE IF EXISTS `companies_industries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies_industries` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(255) DEFAULT NULL,
                                        `description` varchar(255) DEFAULT NULL,
                                        `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                        `updated_at` timestamp NULL,
                                        PRIMARY KEY (`id`),
                                        UNIQUE KEY `companies_industries_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies_industries`
--

LOCK TABLES `companies_industries` WRITE;
/*!40000 ALTER TABLE `companies_industries` DISABLE KEYS */;
INSERT INTO `companies_industries` VALUES (1,'Automotive',NULL,'2020-07-21 13:23:32',NULL),(2,'IT & Tech',NULL,'2020-07-21 13:23:32',NULL);
/*!40000 ALTER TABLE `companies_industries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garage`
--

DROP TABLE IF EXISTS `garage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garage` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `cars` int(11) DEFAULT NULL,
                          `tools` int(11) DEFAULT NULL,
                          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                          `updated_at` timestamp NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `garage_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garage`
--

LOCK TABLES `garage` WRITE;
/*!40000 ALTER TABLE `garage` DISABLE KEYS */;
INSERT INTO `garage` VALUES (1,NULL,NULL,'2020-07-09 11:07:58',NULL);
/*!40000 ALTER TABLE `garage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garage_tools`
--

DROP TABLE IF EXISTS `garage_tools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garage_tools` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `garage` int(11) DEFAULT NULL,
                                `name` varchar(255) DEFAULT NULL,
                                `description` varchar(255) DEFAULT NULL,
                                `location` varchar(255) DEFAULT NULL,
                                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                `updated_at` timestamp NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `garage_tools_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garage_tools`
--

LOCK TABLES `garage_tools` WRITE;
/*!40000 ALTER TABLE `garage_tools` DISABLE KEYS */;
/*!40000 ALTER TABLE `garage_tools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `uuid` varchar(255) DEFAULT NULL,
                          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                          `updated_at` timestamp NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `images_id_uindex` (`id`),
                          UNIQUE KEY `images_uuid_uindex` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `text` text NOT NULL,
                            `sender` int(11) NOT NULL,
                            `send_from` varchar(255) DEFAULT NULL,
                            `locale` varchar(255) DEFAULT NULL,
                            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                            `updated_at` timestamp NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `messages_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'dbsejdrxbdfb',3,'vag','dk','2020-07-11 16:08:32',NULL);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
                              `version` bigint(20) NOT NULL,
                              `migration_name` varchar(100) DEFAULT NULL,
                              `start_time` timestamp NULL DEFAULT NULL,
                              `end_time` timestamp NULL DEFAULT NULL,
                              `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
                              PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications`
--

DROP TABLE IF EXISTS `modifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `meta` int(11) DEFAULT NULL,
                                 `parts` int(11) DEFAULT NULL,
                                 `guides` int(11) DEFAULT NULL,
                                 `comments` int(11) DEFAULT NULL,
                                 `images` int(11) DEFAULT NULL,
                                 `price` int(11) DEFAULT NULL,
                                 `creator` int(11) DEFAULT NULL,
                                 `published` tinyint(1) DEFAULT '0',
                                 `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                 `updated_at` timestamp NULL,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `modifications_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications`
--

LOCK TABLES `modifications` WRITE;
/*!40000 ALTER TABLE `modifications` DISABLE KEYS */;
INSERT INTO `modifications` VALUES (1,NULL,NULL,NULL,NULL,NULL,1200,1,0,'2020-07-10 18:46:40',NULL);
/*!40000 ALTER TABLE `modifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications_comments`
--

DROP TABLE IF EXISTS `modifications_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications_comments` (
                                          `id` int(11) NOT NULL AUTO_INCREMENT,
                                          `modification_id` int(11) DEFAULT NULL,
                                          `starter` int(11) DEFAULT NULL,
                                          `message` text,
                                          `replies` int(11) DEFAULT NULL,
                                          `locale` varchar(255) DEFAULT NULL,
                                          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                          `updated_at` timestamp NULL,
                                          PRIMARY KEY (`id`),
                                          UNIQUE KEY `modifications_comments_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications_comments`
--

LOCK TABLES `modifications_comments` WRITE;
/*!40000 ALTER TABLE `modifications_comments` DISABLE KEYS */;
INSERT INTO `modifications_comments` VALUES (1,1,1,'Hvor købte du alle dele henne?',NULL,'dk','2020-07-11 18:45:20',NULL);
/*!40000 ALTER TABLE `modifications_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications_comments_replies`
--

DROP TABLE IF EXISTS `modifications_comments_replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications_comments_replies` (
                                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `comment_id` int(11) DEFAULT NULL,
                                                  `commentator` int(11) DEFAULT NULL,
                                                  `message` text,
                                                  `locale` varchar(255) DEFAULT NULL,
                                                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                                  `updated_at` timestamp NULL,
                                                  PRIMARY KEY (`id`),
                                                  UNIQUE KEY `modifications_comments_replies_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications_comments_replies`
--

LOCK TABLES `modifications_comments_replies` WRITE;
/*!40000 ALTER TABLE `modifications_comments_replies` DISABLE KEYS */;
INSERT INTO `modifications_comments_replies` VALUES (1,1,1,'Dem købte jeg på eBay','dk','2020-07-11 18:47:59',NULL),(2,1,1,'Kan du huske hvad det hele løb op i?','dk','2020-07-11 18:53:12',NULL);
/*!40000 ALTER TABLE `modifications_comments_replies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications_guides`
--

DROP TABLE IF EXISTS `modifications_guides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications_guides` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `modification_id` int(11) DEFAULT NULL,
                                        `meta` int(11) DEFAULT NULL,
                                        `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                        `updated_at` timestamp NULL,
                                        PRIMARY KEY (`id`),
                                        UNIQUE KEY `modifications_guides_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications_guides`
--

LOCK TABLES `modifications_guides` WRITE;
/*!40000 ALTER TABLE `modifications_guides` DISABLE KEYS */;
INSERT INTO `modifications_guides` VALUES (1,1,NULL,'2020-07-10 19:13:17',NULL);
/*!40000 ALTER TABLE `modifications_guides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications_guides_meta`
--

DROP TABLE IF EXISTS `modifications_guides_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications_guides_meta` (
                                             `id` int(11) NOT NULL AUTO_INCREMENT,
                                             `guide_id` int(11) DEFAULT NULL,
                                             `text` text,
                                             `author` int(11) DEFAULT NULL,
                                             `locale` varchar(255) DEFAULT NULL,
                                             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                             `updated_at` timestamp NULL DEFAULT NULL,
                                             PRIMARY KEY (`id`),
                                             UNIQUE KEY `modifications_guides_meta_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications_guides_meta`
--

LOCK TABLES `modifications_guides_meta` WRITE;
/*!40000 ALTER TABLE `modifications_guides_meta` DISABLE KEYS */;
INSERT INTO `modifications_guides_meta` VALUES (1,1,'egqggwegagraewg',1,'dk','2020-07-11 18:47:59',NULL),(2,1,'wagwg4qawga',1,'gb','2020-07-11 18:47:59',NULL);
/*!40000 ALTER TABLE `modifications_guides_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications_images`
--

DROP TABLE IF EXISTS `modifications_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications_images` (
                                        `id` int(11) NOT NULL AUTO_INCREMENT,
                                        `modification_id` int(11) DEFAULT NULL,
                                        `thumbnail` tinyint(1) DEFAULT NULL,
                                        `path` varchar(255) DEFAULT NULL,
                                        `description` varchar(255) DEFAULT NULL,
                                        `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                        `updated_at` timestamp NULL,
                                        PRIMARY KEY (`id`),
                                        UNIQUE KEY `modifications_images_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications_images`
--

LOCK TABLES `modifications_images` WRITE;
/*!40000 ALTER TABLE `modifications_images` DISABLE KEYS */;
INSERT INTO `modifications_images` VALUES (1,1,1,'bbeb','ntydxng','2020-07-10 19:09:20',NULL);
/*!40000 ALTER TABLE `modifications_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications_meta`
--

DROP TABLE IF EXISTS `modifications_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications_meta` (
                                      `id` int(11) NOT NULL AUTO_INCREMENT,
                                      `modification_id` int(11) DEFAULT NULL,
                                      `title` varchar(255) DEFAULT NULL,
                                      `description` text,
                                      `locale` varchar(255) DEFAULT NULL,
                                      `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                      `updated_at` timestamp NULL,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `modifications_meta_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications_meta`
--

LOCK TABLES `modifications_meta` WRITE;
/*!40000 ALTER TABLE `modifications_meta` DISABLE KEYS */;
INSERT INTO `modifications_meta` VALUES (1,1,'Regnsensor','lorem ipsum','dk','2020-07-10 18:48:12',NULL),(2,1,'Rainsensor','bacon crest ipsom','gb','2020-07-10 18:48:12',NULL);
/*!40000 ALTER TABLE `modifications_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications_parts`
--

DROP TABLE IF EXISTS `modifications_parts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications_parts` (
                                       `id` int(11) NOT NULL AUTO_INCREMENT,
                                       `modification_id` int(11) DEFAULT NULL,
                                       `sku` varchar(255) DEFAULT NULL,
                                       `meta` int(11) DEFAULT NULL,
                                       `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                       `updated_at` timestamp NULL,
                                       PRIMARY KEY (`id`),
                                       UNIQUE KEY `modifications_parts_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications_parts`
--

LOCK TABLES `modifications_parts` WRITE;
/*!40000 ALTER TABLE `modifications_parts` DISABLE KEYS */;
INSERT INTO `modifications_parts` VALUES (1,1,'1K0 903 859 T',NULL,'2020-07-10 18:57:13',NULL),(2,1,'5K0 889 379',NULL,'2020-07-10 19:04:41',NULL);
/*!40000 ALTER TABLE `modifications_parts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modifications_parts_meta`
--

DROP TABLE IF EXISTS `modifications_parts_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modifications_parts_meta` (
                                            `id` int(11) NOT NULL AUTO_INCREMENT,
                                            `part_id` int(11) DEFAULT NULL,
                                            `title` varchar(255) DEFAULT NULL,
                                            `locale` varchar(255) DEFAULT NULL,
                                            `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                            `updated_at` timestamp NULL,
                                            PRIMARY KEY (`id`),
                                            UNIQUE KEY `modifications_parts_meta_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modifications_parts_meta`
--

LOCK TABLES `modifications_parts_meta` WRITE;
/*!40000 ALTER TABLE `modifications_parts_meta` DISABLE KEYS */;
INSERT INTO `modifications_parts_meta` VALUES (1,1,'Regnsensor med auto tunnel-lys','dk','2020-07-10 19:01:19',NULL),(2,1,'Rainsensor with auto tunnel-light','gb','2020-07-10 19:01:50',NULL),(3,2,'Rudebeslag f. regnsensor','dk','2020-07-10 19:05:36',NULL),(4,2,'Bracket, rainsensor','gb','2020-07-10 19:05:36',NULL);
/*!40000 ALTER TABLE `modifications_parts_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monitor_endpoints`
--

DROP TABLE IF EXISTS `monitor_endpoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitor_endpoints` (
                                     `id` int(11) NOT NULL AUTO_INCREMENT,
                                     `uri` text NOT NULL,
                                     `frequency` int(11) NOT NULL DEFAULT '1',
                                     `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                     `updated_at` timestamp NULL,
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `monitor_endpoints_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitor_endpoints`
--

LOCK TABLES `monitor_endpoints` WRITE;
/*!40000 ALTER TABLE `monitor_endpoints` DISABLE KEYS */;
INSERT INTO `monitor_endpoints` VALUES (20,'www.korfitz.com',1,'2020-08-18 19:18:33',NULL);
/*!40000 ALTER TABLE `monitor_endpoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monitor_statuses`
--

DROP TABLE IF EXISTS `monitor_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitor_statuses` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `endpoint_id` int(11) NOT NULL,
                                    `status_code` int(11) NOT NULL,
                                    `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                    `updated_at` timestamp NULL,
                                    PRIMARY KEY (`id`),
                                    UNIQUE KEY `monitor_statuses_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitor_statuses`
--

LOCK TABLES `monitor_statuses` WRITE;
/*!40000 ALTER TABLE `monitor_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `monitor_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
                         `id` int(11) NOT NULL AUTO_INCREMENT,
                         `uid` varchar(255) DEFAULT NULL,
                         `email` varchar(255) DEFAULT NULL,
                         `password` varchar(255) DEFAULT NULL,
                         `2fa_enabled` tinyint(1) DEFAULT '0',
                         `2fa_secret` varchar(255) DEFAULT NULL,
                         `personalization` int(11) DEFAULT NULL,
                         `img_cover` varchar(255) DEFAULT NULL,
                         `img_avatar` varchar(255) DEFAULT 'https://via.placeholder.com/160x160',
                         `role` int(11) DEFAULT '2',
                         `initials` varchar(255) DEFAULT NULL,
                         `first_name` varchar(255) DEFAULT NULL,
                         `last_name` varchar(255) DEFAULT NULL,
                         `contacts` int(11) DEFAULT NULL,
                         `addresses` int(11) DEFAULT NULL,
                         `sso` int(11) DEFAULT NULL,
                         `services` int(11) DEFAULT NULL,
                         `vault` int(11) DEFAULT NULL,
                         `region` int(11) DEFAULT NULL,
                         `status` tinyint(1) DEFAULT NULL,
                         `has_access` int(11) DEFAULT NULL,
                         `token` varchar(255) DEFAULT NULL,
                         `isActivated` tinyint(1) DEFAULT '0',
                         `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                         `updated_at` timestamp NULL,
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `users_id_uindex` (`id`),
                         UNIQUE KEY `users_email_uindex` (`email`),
                         UNIQUE KEY `users_initials_uindex` (`initials`),
                         UNIQUE KEY `users_uid_uindex` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,'stefan@korfitz.com','$2y$10$1tjM2HQPxlt1xqZcXye/N.ZpLLUusVhvvVWORIISJgDeIrfbVbWcK',0,'null',NULL,'https://adminlte.io/themes/AdminLTE/dist/img/photo1.png','https://avatarfiles.alphacoders.com/192/thumb-192479.jpg',5,'skf','Stefan','Korfitz',NULL,NULL,NULL,NULL,NULL,4,1,NULL,NULL,1,'2020-08-15 16:47:34','2020-08-24 10:10:35'),(3,NULL,NULL,NULL,0,NULL,NULL,NULL,'https://via.placeholder.com/160x160',NULL,NULL,'DELETED','USER',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,'2020-07-11 16:33:06',NULL),(5,NULL,NULL,NULL,0,NULL,NULL,NULL,'https://via.placeholder.com/160x160',NULL,NULL,'DELETED','USER',NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,0,'2020-07-12 14:13:11',NULL),(6,NULL,'john@doe.com','$2y$10$HUU7x.Dr0t.e2lRNSn.Kk.3oNcM9dHfR7t3ILuDc7hyItbQMEWmca',0,NULL,NULL,'https://picsum.photos/400/200','https://via.placeholder.com/160x160',2,'','John','Doe',NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,'J7SB-6V56-APBB',0,'2020-07-25 19:18:48',NULL),(11,NULL,'info@bahay.dk','$2y$10$K8HmxOs4BbLtMGTN4WQnnekz10MvBBLhVB0nffdwbiDn/nafXnzdy',0,NULL,NULL,'https://picsum.photos/400/200','https://via.placeholder.com/160x160',2,NULL,'Bahay','Group',NULL,NULL,NULL,NULL,NULL,NULL,3,NULL,'CDCT-6D6R-AUB4',0,'2020-07-25 19:53:27',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_access`
--

DROP TABLE IF EXISTS `users_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_access` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `user_id` int(11) DEFAULT NULL,
                                `dashboard` tinyint(1) DEFAULT '0',
                                `nexus` tinyint(1) DEFAULT '0',
                                `vag` tinyint(1) DEFAULT '0',
                                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                `updated_at` timestamp NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `users_access_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_access`
--

LOCK TABLES `users_access` WRITE;
/*!40000 ALTER TABLE `users_access` DISABLE KEYS */;
INSERT INTO `users_access` VALUES (1,1,1,1,1,'2020-08-07 13:55:29',NULL);
/*!40000 ALTER TABLE `users_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_addresses`
--

DROP TABLE IF EXISTS `users_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_addresses` (
                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                   `user_id` int(11) DEFAULT NULL,
                                   `address_id` int(11) DEFAULT NULL,
                                   `is_shipping_address` tinyint(1) DEFAULT NULL,
                                   `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                   `updated_at` timestamp NULL,
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `users_addresses_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_addresses`
--

LOCK TABLES `users_addresses` WRITE;
/*!40000 ALTER TABLE `users_addresses` DISABLE KEYS */;
INSERT INTO `users_addresses` VALUES (1,1,1,0,'2020-07-11 13:44:14',NULL),(2,1,2,1,'2020-07-11 13:44:14',NULL);
/*!40000 ALTER TABLE `users_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_contacts`
--

DROP TABLE IF EXISTS `users_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_contacts` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `user_id` int(11) DEFAULT NULL,
                                  `tel_home` varchar(255) DEFAULT NULL,
                                  `tel_mobile` varchar(255) DEFAULT NULL,
                                  `tel_work` varchar(255) DEFAULT NULL,
                                  `accepts_sms` tinyint(1) DEFAULT '0' COMMENT 'Tells wether or not the user, has accepted to receive any text messages from you /- the system',
                                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                  `updated_at` timestamp NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `users_contacts_id_uindex` (`id`),
                                  UNIQUE KEY `users_contacts_user_id_uindex` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_contacts`
--

LOCK TABLES `users_contacts` WRITE;
/*!40000 ALTER TABLE `users_contacts` DISABLE KEYS */;
INSERT INTO `users_contacts` VALUES (1,1,'','50 13 40 33','82 30 70 60',0,'2020-08-03 10:44:09',NULL),(3,5,'...','...','...',0,'2020-07-12 07:01:13',NULL),(4,6,NULL,NULL,NULL,NULL,'2020-07-25 19:18:48',NULL),(5,11,NULL,NULL,NULL,NULL,'2020-07-25 19:53:27',NULL);
/*!40000 ALTER TABLE `users_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_personalizations`
--

DROP TABLE IF EXISTS `users_personalizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_personalizations` (
                                          `id` int(11) NOT NULL AUTO_INCREMENT,
                                          `user_id` int(11) DEFAULT NULL,
                                          `dashboard` int(11) DEFAULT NULL,
                                          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                          `updated_at` timestamp NULL,
                                          PRIMARY KEY (`id`),
                                          UNIQUE KEY `users_personalizations_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_personalizations`
--

LOCK TABLES `users_personalizations` WRITE;
/*!40000 ALTER TABLE `users_personalizations` DISABLE KEYS */;
INSERT INTO `users_personalizations` VALUES (1,1,1,'2020-08-03 08:49:21',NULL);
/*!40000 ALTER TABLE `users_personalizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_personalizations_dashboard`
--

DROP TABLE IF EXISTS `users_personalizations_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_personalizations_dashboard` (
                                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                                    `layout_fixedHeader` tinyint(1) DEFAULT NULL,
                                                    `layout_fixedSidebar` tinyint(1) DEFAULT NULL,
                                                    `layout_fixedFooter` tinyint(1) DEFAULT NULL,
                                                    `colors_header` varchar(255) DEFAULT NULL,
                                                    `colors_sidebar` varchar(255) DEFAULT NULL,
                                                    `image_sidebar` varchar(255) DEFAULT NULL,
                                                    `image_sidebar_opacity` varchar(255) DEFAULT NULL,
                                                    `main_pageSectionTabs` varchar(255) DEFAULT NULL,
                                                    `main_lightColorScheme` varchar(255) DEFAULT NULL,
                                                    `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                                    `updated_at` timestamp NULL,
                                                    PRIMARY KEY (`id`),
                                                    UNIQUE KEY `users_personalizations_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_personalizations_dashboard`
--

LOCK TABLES `users_personalizations_dashboard` WRITE;
/*!40000 ALTER TABLE `users_personalizations_dashboard` DISABLE KEYS */;
INSERT INTO `users_personalizations_dashboard` VALUES (1,1,1,0,'bg-mean-fruit header-text-dark','bg-focus sidebar-text-light','city4','opacity-2','body-tabs-line','app-theme-grey','2020-08-23 16:42:06','2020-08-23 16:45:42'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-08-23 16:44:24',NULL);
/*!40000 ALTER TABLE `users_personalizations_dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_region`
--

DROP TABLE IF EXISTS `users_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_region` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `area` varchar(255) DEFAULT NULL,
                                `description` varchar(255) DEFAULT NULL,
                                `code` int(11) DEFAULT NULL,
                                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                `updated_at` timestamp NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `users_region_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_region`
--

LOCK TABLES `users_region` WRITE;
/*!40000 ALTER TABLE `users_region` DISABLE KEYS */;
INSERT INTO `users_region` VALUES (1,'Other',NULL,0,'2020-07-09 08:18:46',NULL),(2,'North America',NULL,1,'2020-07-09 08:18:46',NULL),(3,'Latin America',NULL,2,'2020-07-09 08:18:46',NULL),(4,'Europe (nordic)','Denmark, Finland, Norway, Sweden',3,'2020-07-09 08:18:46',NULL),(5,'Europe (baltic)',NULL,4,'2020-07-09 08:18:46',NULL),(6,'Europe (western)',NULL,5,'2020-07-09 08:18:46',NULL),(7,'Russia',NULL,6,'2020-07-09 08:18:46',NULL),(8,'Middle East',NULL,7,'2020-07-09 08:18:46',NULL),(9,'Africa',NULL,8,'2020-07-09 08:18:46',NULL),(10,'Southeast Asia',NULL,9,'2020-07-09 08:18:46',NULL),(11,'China',NULL,10,'2020-07-09 08:18:46',NULL),(12,'Australia (pacific)',NULL,11,'2020-07-09 08:18:46',NULL);
/*!40000 ALTER TABLE `users_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_role`
--

DROP TABLE IF EXISTS `users_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_role` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `name` varchar(255) DEFAULT NULL,
                              `description` text,
                              `permissions_level` int(11) DEFAULT NULL,
                              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                              `updated_at` timestamp NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `users_role_id_uindex` (`id`),
                              UNIQUE KEY `users_role_permissions_level_uindex` (`permissions_level`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_role`
--

LOCK TABLES `users_role` WRITE;
/*!40000 ALTER TABLE `users_role` DISABLE KEYS */;
INSERT INTO `users_role` VALUES (1,'Guest','...',0,'2020-07-09 08:15:26',NULL),(2,'Member','...',1,'2020-07-09 08:15:26',NULL),(3,'Premium','...',2,'2020-07-09 08:15:26',NULL),(4,'Admin','...',3,'2020-07-11 12:01:21',NULL),(5,'Shop Keeper','...',4,'2020-07-11 12:01:21',NULL);
/*!40000 ALTER TABLE `users_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_services`
--

DROP TABLE IF EXISTS `users_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_services` (
                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                  `user_id` int(11) DEFAULT NULL,
                                  `service` varchar(255) DEFAULT NULL,
                                  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                  `updated_at` timestamp NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `users_services_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_services`
--

LOCK TABLES `users_services` WRITE;
/*!40000 ALTER TABLE `users_services` DISABLE KEYS */;
INSERT INTO `users_services` VALUES (1,1,'nexus','2020-07-11 11:57:06',NULL),(2,1,'dashboard','2020-07-11 11:57:06',NULL),(3,1,'portfolio','2020-07-11 11:57:06',NULL),(4,1,'vag','2020-07-11 11:57:06',NULL),(5,6,'vag','2020-07-25 19:18:48',NULL),(6,11,'dashboard','2020-07-25 19:53:27',NULL);
/*!40000 ALTER TABLE `users_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_socials`
--

DROP TABLE IF EXISTS `users_socials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_socials` (
                                 `id` int(11) NOT NULL AUTO_INCREMENT,
                                 `user_id` int(11) DEFAULT NULL,
                                 `service` varchar(255) DEFAULT NULL,
                                 `username` varchar(255) DEFAULT NULL,
                                 `name` varchar(255) DEFAULT NULL,
                                 `email` varchar(255) DEFAULT NULL,
                                 `photo` varchar(255) DEFAULT NULL,
                                 `uid` varchar(255) DEFAULT NULL,
                                 `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                 `updated_at` timestamp NULL,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `users_socials_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_socials`
--

LOCK TABLES `users_socials` WRITE;
/*!40000 ALTER TABLE `users_socials` DISABLE KEYS */;
INSERT INTO `users_socials` VALUES (1,1,'Facebook',NULL,NULL,NULL,NULL,'vGfFspi5f2gxoSFrGPIS0psM2dr1','2020-07-11 12:03:08',NULL),(2,NULL,'Github',NULL,NULL,NULL,NULL,'QExxaotFrKROmo9nBtZrUgy6POl1','2020-07-11 12:03:08',NULL),(3,1,'Google',NULL,NULL,NULL,NULL,'BFPIHs6MrVV32E5J9VEuw4o4FRu1','2020-08-23 15:39:11',NULL);
/*!40000 ALTER TABLE `users_socials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_status`
--

DROP TABLE IF EXISTS `users_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_status` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `title` varchar(255) DEFAULT NULL,
                                `description` varchar(255) DEFAULT NULL,
                                `status_level` int(11) DEFAULT NULL,
                                `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                `updated_at` timestamp NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `users_status_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_status`
--

LOCK TABLES `users_status` WRITE;
/*!40000 ALTER TABLE `users_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vaults`
--

DROP TABLE IF EXISTS `vaults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vaults` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `user_id` int(11) DEFAULT NULL,
                          `categories` int(11) DEFAULT NULL,
                          `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                          `updated_at` timestamp NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `vault_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vaults`
--

LOCK TABLES `vaults` WRITE;
/*!40000 ALTER TABLE `vaults` DISABLE KEYS */;
INSERT INTO `vaults` VALUES (1,1,NULL,'2020-07-30 18:04:53',NULL);
/*!40000 ALTER TABLE `vaults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vaults_categories`
--

DROP TABLE IF EXISTS `vaults_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vaults_categories` (
                                     `id` int(11) NOT NULL AUTO_INCREMENT,
                                     `vault_id` int(11) DEFAULT NULL,
                                     `category_id` int(11) DEFAULT NULL,
                                     `entries` int(11) DEFAULT NULL,
                                     `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                     `updated_at` timestamp NULL,
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `vault_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vaults_categories`
--

LOCK TABLES `vaults_categories` WRITE;
/*!40000 ALTER TABLE `vaults_categories` DISABLE KEYS */;
INSERT INTO `vaults_categories` VALUES (1,1,1,NULL,'2020-07-30 18:05:52',NULL),(2,1,2,NULL,'2020-07-30 18:05:59',NULL);
/*!40000 ALTER TABLE `vaults_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vaults_categories_entries`
--

DROP TABLE IF EXISTS `vaults_categories_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vaults_categories_entries` (
                                             `id` int(11) NOT NULL AUTO_INCREMENT,
                                             `category_id` int(11) DEFAULT NULL,
                                             `field_1` varchar(255) DEFAULT NULL,
                                             `field_2` varchar(255) DEFAULT NULL,
                                             `field_3` varchar(255) DEFAULT NULL,
                                             `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                             `updated_at` timestamp NULL,
                                             PRIMARY KEY (`id`),
                                             UNIQUE KEY `vault_categories_entries_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vaults_categories_entries`
--

LOCK TABLES `vaults_categories_entries` WRITE;
/*!40000 ALTER TABLE `vaults_categories_entries` DISABLE KEYS */;
INSERT INTO `vaults_categories_entries` VALUES (1,1,'ethtrhwh','4wj',NULL,'2020-07-31 15:56:56',NULL),(2,1,'rsthrw','4jw','yyetjd','2020-07-31 15:56:56',NULL),(3,2,'rtrj','wjw','fxge','2020-07-31 15:56:56',NULL),(4,2,'rtjwej','rsjt','sazg','2020-07-31 15:56:56',NULL),(5,2,'rwtj','wrj','ws','2020-07-31 15:56:56',NULL);
/*!40000 ALTER TABLE `vaults_categories_entries` ENABLE KEYS */;
UNLOCK TABLES;
/*!50112 SET @disable_bulk_load = IF (@is_rocksdb_supported, 'SET SESSION rocksdb_bulk_load = @old_rocksdb_bulk_load', 'SET @dummy_rocksdb_bulk_load = 0') */;
/*!50112 PREPARE s FROM @disable_bulk_load */;
/*!50112 EXECUTE s */;
/*!50112 DEALLOCATE PREPARE s */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-24 12:10:45
