<?php

namespace app\console;

use GuzzleHttp\Client;

use Interop\Container\ContainerInterface;

use Noodlehaus\Config;

use Symfony\Component\{
    Console\Command\Command,
    Console\Input\InputInterface,
    Console\Output\OutputInterface,
    EventDispatcher\EventDispatcher
};

abstract class Commands extends Command {

    protected $c;
    protected $client;
    protected $dispatcher;
    protected $env;
    protected $input;
    protected $output;
    protected $getOption;
    protected $command;
    protected $description;

    public function __construct(ContainerInterface $c) {

        parent::__construct();
        $this->c = $c;
        $this->client = $c->get(Client::class);
        $this->dispatcher = $c->get(EventDispatcher::class);
        $this->env = $c->get(Config::class);
        $this->setName($this->command);
        $this->setDescription($this->description);
    }

    protected function configure() {

        $this->setName($this->command)->setDescription($this->description);
        $this->addArguments();
        $this->addOptions();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $this->input = $input;
        $this->output = $output;

        return $this->handle($input, $output);
    }

    protected function argument($name) {

        return $this->input->getArgument($name);
    }

    protected function option($name) {

        return $this->input->getOption($name);
    }

    protected function addArguments() {

        foreach ($this->arguments() as $argument) {
            $this->addArgument($argument[0], $argument[1], $argument[2]);
        }
    }

    protected function addOptions() {

        foreach ($this->options() as $option) {
            $this->addOption($option[0], $option[1], $option[2], $option[3], $option[4]);
        }
    }

    protected function info($value) {

        return $this->output->writeln('<info>' . $value . '</info>');
    }

    protected function error($value) {

        return $this->output->writeln('<error>' . $value . '</error>');
    }
}