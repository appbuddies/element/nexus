<?php

namespace app\console;

use app\console\commands\{
    DisplayAppModeCommand,
    StartServerCommand
};

use app\console\commands\generators\{
    ConsoleGeneratorCommand,
    ControllerGeneratorCommand,
    DataModelGeneratorCommand
};

use app\console\commands\locales\{
    ActivateLocaleCommand,
    AddLocaleCommand,
    DeactivateLocaleCommand,
    ListLocalesCommand
};

use app\console\commands\migrations\{
    Create,
    Migrate,
    Rollback,
    SeedCreate,
    SeedRun,
    Status
};

use app\console\commands\monitor\{
    Run,
    AddEndpointCommand,
    RemoveEndpointCommand,
    StatusCommand,
    UpdateFrequencyCommand
};

use app\console\commands\tests\phpunit\{
    RunTestsCommand
};

use app\console\commands\users\{
    ActivateUserCommand,
    AddUserCommand,
    DeactivateUserCommand,
    ListUserRolesCommand,
    ShowUsersCommand
};

class Kernel {

    protected $defaultCommands = [
        /* APPLICATION */
        DisplayAppModeCommand::class,
        StartServerCommand::class,
        /* MAKE */
        ConsoleGeneratorCommand::class,
        ControllerGeneratorCommand::class,
        DataModelGeneratorCommand::class,
        /* LOCALES */
        ActivateLocaleCommand::class,
        AddLocaleCommand::class,
        DeactivateLocaleCommand::class,
        ListLocalesCommand::class,
        /* MIGRATIONS */
        Create::class,
        Migrate::class,
        Rollback::class,
        SeedCreate::class,
        SeedRun::class,
        Status::class,
        /* MONITOR */
        Run::class,
        StatusCommand::class,
        AddEndpointCommand::class,
        RemoveEndpointCommand::class,
        UpdateFrequencyCommand::class,
        /* TESTS -> PHPUNIT */
        RunTestsCommand::class,
        /* USERS */
        AddUserCommand::class,
        ActivateUserCommand::class,
        DeactivateUserCommand::class,
        ListUserRolesCommand::class,
        ShowUsersCommand::class
    ];

    protected $commands = [

        /*
         * ...Feel free to add your own commands
         */

    ];

    public function getCommands() {

        return array_merge($this->defaultCommands, $this->commands);
    }
}
