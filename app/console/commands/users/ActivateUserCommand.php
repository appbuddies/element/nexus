<?php

namespace app\console\commands\users;

use app\models\data\User;

use Symfony\Component\{
    Console\Input\InputOption,
    Console\Input\InputArgument,
    Console\Input\InputInterface,
    Console\Output\OutputInterface
};

use app\console\Commands;

class ActivateUserCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'user:on';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Activate a given user.';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return void
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $user = User::with([])->where('id', '=', $this->argument('id'))->first();

        if($user) {

            if($user->isActivated == "0") {

                $user->isActivated = "1";
                $user->save();

                $output->writeln("<info>{$user->email} activated</info>");

            } else {

                $output->writeln("<error>{$user->email} already active</error>");
            }

        } else {

            $output->writeln("<error>{$user->email} doesn't exist in database</error>");
        }
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            ['id', InputArgument::REQUIRED, 'User ID'],
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            //
        ];
    }
}
