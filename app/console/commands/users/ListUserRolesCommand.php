<?php

namespace app\console\commands\users;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use app\console\Commands;

use app\models\data\UserRole;

class ListUserRolesCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'user:roles';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'List possible user roles.';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return void
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $roles = UserRole::all();

        if($roles) {

            foreach ($roles as $role) {

                $output->writeln("\n| id: <info>{$role->id}</info>\n|--------------------------------------------------------------------|\n| role:  <info>{$role->name}</info>\n| desc:  <info>{$role->description}</info>\n| level: <info>{$role->permissions_level}</info>\n|--------------------------------------------------------------------|");
            }

        } else {

            $output->writeln("<error>No users exists in database!?</error>");
        }
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            //
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            //
        ];
    }
}
