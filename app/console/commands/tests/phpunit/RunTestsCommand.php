<?php

namespace app\console\commands\tests\phpunit;

use app\console\Commands;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class RunTestsCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'phpunit:run';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Run your phpunit tests';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $process = new Process(

            /**
             * path to command
             * --option
             * argument
             * etc.
             */
            [
                './vendor/bin/phpunit',
                '--colors=always'
            ]
        );

        $process->run();

        /**
         * executes after the command finishes
         * and throws an exception if not successful
         */
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $output->writeln($process->getOutput());
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            //
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
//            [
//                'all',
//                'a',
//                InputOption::VALUE_OPTIONAL,
//                'Choose if you want to run ALL tests, or specifies which ones to run?',
//                true
//            ]
        ];
    }
}
