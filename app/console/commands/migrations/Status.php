<?php

namespace app\console\commands\migrations;

use Phinx\Console\PhinxApplication;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use app\console\Commands;

class Status extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'data:migration-status';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Show migration status';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return int
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $phinx = new PhinxApplication();
        $command = $phinx->find('status');

        $arguments = [
            'command' => 'status'
        ];

        $greetInput = new ArrayInput($arguments);

        try {

            $command->run($greetInput, $output);

        } catch (\Exception $e) {

            dump($e);

        }

        return 0;
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            // ...
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            [
                'environment',
                'e',
                InputOption::VALUE_OPTIONAL,
                'The target environment',
                'development'
            ],
            [
                'format',
                'f',
                InputOption::VALUE_OPTIONAL,
                'The output format: text or json. Defaults to text',
                'text'
            ]
        ];
    }
}
