<?php

namespace app\console\commands\migrations;

use Phinx\Console\PhinxApplication;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use app\console\Commands;

class Create extends Commands {

    /**
     * The name of the interface that any external template creation class is required to implement.
     */
    const CREATION_INTERFACE = 'Phinx\Migration\CreationInterface';

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'data:create-table';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Create a new database migration table';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return int
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $phinx = new PhinxApplication();
        $command = $phinx->find('create');

        $arguments = [
            'command' => 'create',
            'name'    => $input->getArgument('name')
        ];

        $greetInput = new ArrayInput($arguments);

        try {

            $command->run($greetInput, $output);

        } catch (\Exception $e) {

            dump($e);

        }

        return 0;
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'What is the name of the migration (in CamelCase)?'
            ]
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            [
                'template',
                't',
                InputOption::VALUE_OPTIONAL,
                'Use an alternative template',
                ''
            ],
            [
                'class',
                'c',
                InputOption::VALUE_OPTIONAL,
                'Use a class implementing "' . self::CREATION_INTERFACE . '" to generate the template',
                ''
            ],
            [
                'path',
                null,
                InputOption::VALUE_OPTIONAL,
                'Specify the path in which to create this migration',
                ''
            ]
        ];
    }
}
