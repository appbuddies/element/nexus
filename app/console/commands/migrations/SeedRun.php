<?php

namespace app\console\commands\migrations;

use Phinx\Console\PhinxApplication;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use app\console\Commands;

class SeedRun extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'data:seed-run';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Run database seeders';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return int
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $phinx = new PhinxApplication();
        $command = $phinx->find('seed:run');

        $arguments = [
            'command' => 'seed:run'
        ];

        $greetInput = new ArrayInput($arguments);

        try {

            $command->run($greetInput, $output);

        } catch (\Exception $e) {

            dump($e);

        }

        return 0;
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            // ...
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            [
                'environment',
                'e',
                InputOption::VALUE_OPTIONAL,
                'The target environment',
                'development'
            ],
            [
                'seed',
                's',
                InputOption::VALUE_REQUIRED,
                'What is the name of the seeder?',
                null
            ],
        ];
    }
}
