<?php

namespace app\console\commands\locales;

use app\models\data\Locale;

use Symfony\Component\{
    Console\Input\InputOption,
    Console\Input\InputArgument,
    Console\Input\InputInterface,
    Console\Output\OutputInterface
};

use app\console\Commands;

class AddLocaleCommand extends Commands {

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'locale:add';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Add locale to database.';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return void
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $locale = Locale::with([])->where('code', '=', $this->argument('code'))->first();

        if($locale) {

            $output->writeln("<error>{$this->argument('code')} already exists in database!</error>");

        } else {

            $locale = Locale::with([])->firstOrCreate([

                'code'      => $this->argument('code')

            ], [

                'name'      => $this->argument('name'),
                'code'      => $this->argument('code'),
                'activated' => $input->getOption('activated')
            ]);

            $output->writeln("<info>{$this->argument('code')} added to database</info>");
        }
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            ['name', InputArgument::REQUIRED, 'Language name'],
            ['code', InputArgument::REQUIRED, 'Language code'],
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            [
                'activated',
                'a',
                InputOption::VALUE_OPTIONAL,
                'Should the new language be activated or not?',
                false
            ],
        ];
    }
}
