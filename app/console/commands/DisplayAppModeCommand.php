<?php

namespace app\console\commands;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use app\console\Commands;

class DisplayAppModeCommand extends Commands {

    /*
    |---------------------------------------------------------------
    | TODO
    |---------------------------------------------------------------
    |
    | Don't forget to add your newly created console-command to the
    | $commands[] in Kernel.php (you can find it in : app\console),
    | so that the Element cli can start to use it!
    |
    */

    /**
     * The command name.
     *
     * @var string
     */
    protected $command = 'app:mode';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Shows which mode your app is currently running in?';

    /**
     * Handle the command.
     *
     * @param  InputInterface $input
     * @param  OutputInterface $output
     *
     * @return void
     */
    public function handle(InputInterface $input, OutputInterface $output) {

        $mode = getenv("APP_MODE");

        $output->writeln("App-mode is currently set to : <info>{$mode}</info>");
    }

    /**
     * Command arguments
     *
     * @return array
     */
    protected function arguments() {

        /**
         *  name
         *  mode
         *  description
         */
        return [
            //
        ];
    }

    /**
     * Command options.
     *
     * @return array
     */
    protected function options() {

        /**
         *  name
         *  shortcut
         *  mode
         *  description
         *  default
         */
        return [
            //
        ];
    }
}
