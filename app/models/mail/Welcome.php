<?php

namespace app\models\mail;

use app\handlers\{
    mailer\Mailable,
};

use app\models\{
    data\User,
};

use Illuminate\Translation\Translator;

class Welcome extends Mailable {

    protected $translator;
    protected $user;

    /**
     * NewToken constructor.
     *
     * @param Translator $translator
     * @param User $user
     */
    public function __construct(User $user, Translator $translator) {

        $this->user = $user;
        $this->translator = $translator;
    }

    public function build() {

        return $this->subject("{$this->translator->get('mail.auth.new_token_greet')} {$this->user->name}. {$this->translator->get('mail.auth.welcome_title')}")
            ->view('/@shared/templates/mail/welcome.twig')
            ->attach(__DIR__ . '/../../../README.md')
            ->with([
                'user' => $this->user
            ]);
    }
}