<?php

namespace app\models\data;

use Illuminate\Database\Eloquent\Model;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use app\models\data\User as User;

class UserSocial extends Model {

	/**
	 * Making sure, that our model-class is
	 * referring to the correct table !?
	 */
	protected $table = 'users_socials';

	/**
	 * Specifying which columns, we want to write to...
	 */
	protected $fillable = [

	    'id',
        'user_id',
		'service',
        'username',
		'name',
        'email',
        'photo',
        'uid'
	];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {

        return $this->hasOne(User::class);
    }

}
