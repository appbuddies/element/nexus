<?php

namespace app\models\data;

use app\handlers\auth\contracts\JwtSubject;

use Illuminate\Database\Eloquent\Model;

class User extends Model implements JwtSubject {

	/**
	 * Making sure, that our model-class is
	 * referring to the correct table !?
	 */
	protected $table = 'users';

	/**
	 * Specifying which columns, we want to write to...
     *
     * @var array
	 */
	protected $fillable = [

        'uid',
        'email',
		'password',
        'img_cover',
        'img_avatar',
        'role',
        'initials',
        'first_name',
        'last_name',
        'img_cover',
		'img_avatar',
        'sso',
        'status',
        'token',
        'isActivated'
	];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function role() {

        return $this->belongsTo(UserRole::class, 'role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sso() {

        return $this->hasMany(UserSocial::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function status() {

        return $this->belongsTo(UserStatus::class, 'status');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contacts() {

        return $this->hasOne(UserContact::class);
    }

    /**
     * update -> Password
     *
     * @param $password
     */
    public function updatePassword($password) {

        $this->update([

            'password' => password_hash($password, PASSWORD_DEFAULT)
        ]);
    }

    /**
     * update -> Password
     *
     * @param $password
     */
    public function updateActivationToken($token) {

        $this->update([

            'token' => $token
        ]);
    }

    /**
     * @return mixed
     */
    public function getJwtIdentifier() {

        return $this->id;
    }
}
