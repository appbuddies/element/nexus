<?php

namespace app\providers\auth;

interface JwtAuthServiceProvider {

    public function byCredentials($username, $password);
    public function byId($id);
}
