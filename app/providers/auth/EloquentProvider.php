<?php

namespace app\providers\auth;

use app\models\data\User;

class EloquentProvider implements JwtAuthServiceProvider {

    public function byCredentials($email, $password) {

        if (!$user = User::with([])->where('email', '=', $email)->first()) {

            return null;
        }

        if (!password_verify($password, $user->password)) {

            return null;
        }

        return $user;
    }

    public function byId($id) {

        return User::find($id);
    }
}