<?php

use function DI\get;

use app\handlers\{
    app\AppVersion,
    auth\Auth,
    auth\AuthSocial,
    auth\JwtAuth,
    auth\JwtClaims,
    auth\JwtFactory,
    auth\Parser,
    auth\social\Github,
    errors\NotFoundHandler,
    mailer\Mailer,
    notifier\Slack as SlackNotifier,
    notifier\Twilio as TwilioNotifier
};

use app\listeners\{
    endpoint\Slack_DownNotification,
    endpoint\Slack_UpNotification,
    endpoint\SMS_DownNotification,
    endpoint\SMS_UpNotification
};

use app\providers\{
    auth\EloquentProvider,
    jwt\FirebaseProvider
};

use app\validations\{
    Validator,
    contracts\ValidatorInterface,
    users\domain\DomainCheck,
    users\email\EmailAvailable,
    users\initials\InitialsAvailable,
    users\password\MatchesPassword,
    users\password\ConfirmPassword
};

use app\views\{
    Factory,
    extensions\GetEnvExtension,
    extensions\GetYamlExtension,
    extensions\TranslationExtension
};

use GuzzleHttp\Client as HttpClient;

use Illuminate\{
    Translation\Translator,
    Translation\FileLoader,
    Filesystem\Filesystem
};

use Intervention\Image\ImageManager;

use Kreait\Firebase\Factory as Firebase;

use Noodlehaus\Config;

use Psr\Container\{
    ContainerInterface
};

use Psr\Http\Message\{
    ServerRequestInterface,
    ResponseInterface
};

use Slim\{
    Csrf\Guard,
    Flash\Messages,
    Router,
    Views\Twig,
    Views\TwigExtension
};

use Symfony\{
    Component\Console\Application,
    Component\EventDispatcher\EventDispatcher
};

use Twilio\Rest\Client as Twilio;

/**
 * attaching : TO CONTAINER ->
 */
return [

    /* 404 ERROR */
    'notFoundHandler' => function(ContainerInterface $container) {

        return new NotFoundHandler ($container->get(Twig::class));
    },

    /* APP -> CONFIGS */
    Config::class => function() {

        switch (getenv("APP_MODE")) {

            case 'development':
                return new Config(
                    __DIR__ . '/../config/app/development'
                );
                break;

            case 'production':
                return new Config(
                    __DIR__ . '/../config/app/production'
                );
                break;

            default:
                return new Config(
                    __DIR__ . '/../config/app/_examples'
                );
        }
    },

    /* AUTH */
    Auth::class => function () {

        return new Auth;
    },

    /* AUTH -> JWT */
    JwtAuth::class => function (ContainerInterface $container) {

        $authProvider = new EloquentProvider();
        $claimsFactory = new JwtClaims(

            $container->get(ContainerInterface::class),
            $container->get('request')
        );

        $jwtProvider = new FirebaseProvider($container->get(ContainerInterface::class));

        $factory = new JwtFactory($claimsFactory, $jwtProvider);

        $parser = new Parser($jwtProvider);

        return new JwtAuth($authProvider, $factory, $parser);
    },

    /* AUTH -> SOCIAL */
    AuthSocial::class => function () {

        return new AuthSocial;
    },

    /* AUTH -> Social -> Github */
    Github::class => function (ContainerInterface $c) {

        $client = $c->get(HttpClient::class);
        $config = $c->get(Config::class);

        return new Github($client, $config);
    },

    /* Endpoint Monitor -> Symfony EventDispatcher */
    EventDispatcher::class => function (ContainerInterface $container) {

        $dispatcher = new Symfony\Component\EventDispatcher\EventDispatcher();

        $SlackNotificationsIsEnabled = $container->get(Config::class)->get('el.endpoint.notify.slack');
        $SmsNotificationsIsEnabled = $container->get(Config::class)->get('el.endpoint.notify.sms');

        if ($SlackNotificationsIsEnabled) {

            $dispatcher->addListener('endpoint.down', [new Slack_DownNotification($container->get(SlackNotifier::class)), 'handle']);

            $dispatcher->addListener('endpoint.up', [new Slack_UpNotification($container->get(SlackNotifier::class)), 'handle']);

        }

        if ($SmsNotificationsIsEnabled) {

            $dispatcher->addListener('endpoint.down', [new SMS_DownNotification($container->get(TwilioNotifier::class)), 'handle']);

            $dispatcher->addListener('endpoint.up', [new SMS_UpNotification($container->get(TwilioNotifier::class)), 'handle']);

        }

        return $dispatcher;
    },

    /* FIREBASE */
    Firebase::class => function (ContainerInterface $container) {

        $config = $container->get(Config::class)->get('fb.service_account');

        $firebase = new Firebase();
        $firebase->withServiceAccount($config);

        return $firebase;
    },

    /* CSRF */
    Guard::class => function () {

        $guard = new Guard();
        $guard->setPersistentTokenMode(true);

        return $guard;
    },

    /* FLASH */
    Messages::class => function () {

        return new Messages;
    },

    /* Guzzle HttpClient */
    HttpClient::class => function () {

        return new HttpClient();
    },

    /* MAILER */
    Mailer::class => function (ContainerInterface $container) {

        $transport = (new Swift_SmtpTransport($container->get(Config::class)->get('service.ms.host'), $container->get(Config::class)->get('service.ms.port')))
            ->setUsername($container->get(Config::class)->get('service.ms.username'))
            ->setPassword($container->get(Config::class)->get('service.ms.password'));

        $swift = (new Swift_Mailer($transport));

        return (new Mailer($swift, $container->get(Twig::class)))
            ->alwaysFrom($container->get(Config::class)->get('service.ms.from.address'));
    },

    /* NOTIFIERS -> SLACK */
    SlackNotifier::class => function (ContainerInterface $container) {

        $client = $container->get(HttpClient::class);
        $config = $container->get(Config::class);
        $sms = $container->get(Twilio::class);

        return new SlackNotifier($client, $config, $sms);
    },

    /* NOTIFIERS -> TWILIO */
    TwilioNotifier::class => function (ContainerInterface $container) {

        $client = $container->get(HttpClient::class);
        $config = $container->get(Config::class);
        $sms = $container->get(Twilio::class);

        return new TwilioNotifier($client, $config, $sms);
    },

    /* ROUTER */
    'router' => get(Router::class),

    /* TRANSLATOR */
    Translator::class => function (ContainerInterface $container) {

        $fallback = $container->get(Config::class)->get('i18n.translations.fallback');

        $loader = new FileLoader(
            new Filesystem(), $container->get(Config::class)->get('i18n.translations.path')
        );

        $translator = new Illuminate\Translation\Translator($loader, $_SESSION['lang'] ?? $fallback);
        $translator->setFallback($fallback);

        return $translator;
    },

    /* TWIG */
    Twig::class => function(ContainerInterface $container) {

        $config = $container->get(Config::class);

        $twig = Factory::getEngine($config);

        /* EXTENSIONS */
        $twig->addExtension(new TwigExtension(

            $container->get('router'),
            $container->get('request')->getUri()
        ));

        $twig->addExtension(new GetEnvExtension());

        $twig->addExtension(new GetYamlExtension(
            $container->get(Config::class)
        ));

        $twig->addExtension(new TranslationExtension(
            $container->get(Translator::class)
        ));

        /* GLOBALS */
        $twig->getEnvironment()->addGlobal('session', $_SESSION);

        $twig->getEnvironment()->addGlobal('app_version', $container->get(AppVersion::class)->getVersion());

        $twig->getEnvironment()->addGlobal('flash', $container->get(Messages::class));

        $twig->getEnvironment()->addGlobal('auth', [

            'check' => $container->get(Auth::class)->check(),
            'user' => $container->get(Auth::class)->user(),
        ]);

        return $twig;
    },

    /* TWILIO */
    Twilio::class => function (ContainerInterface $container) {

        $config = $container->get(Config::class)->get('service.ss');

        return new Twilio(
            $config['sid'], $config['token']
        );
    },

    /* UPLOAD -> IMAGES */
    ImageManager::class => function (ContainerInterface $container) {

        $manager = new ImageManager();
        $manager->configure($container->get('settings.images'));

        return $manager;
    },

    /* VALIDATOR */
    ValidatorInterface::class => function () {

        return new Validator;
    },

];