<?php

namespace app\events\contracts;

interface Handler {

    public function handle($event);
}
