<?php

namespace app\controllers;

use app\models\{
    data\Locale,
    data\User,
    data\UserAddresses,
    data\UserContact,
    data\UserSocial,
    data\UserStatus,
    mail\NewToken,
    mail\Welcome
};

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use Respect\Validation\Validator as v;

class UserController extends BaseController {

    /**
     * @param Response $response
     * @param Locale $locale
     * @param User $user
     * @param UserStatus $userStatus
     *
     * @return Response
     */
    public function getUsers(Response $response, Locale $locale, User $user, UserStatus $userStatus) {

        $locales    = $locale->with([])->where('activated', '=', true)->get();
        $users      = $user->with(['role'])->get();
        $userStatus = $userStatus->with([])->where('id', '=', $this->auth->user()->status)->first();

        return $this->view->render($response, '/back/other/users/Catalogue.twig', [

            'locales'   => $locales,
            'users'     => $users,
            'userStatus'=> $userStatus,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     *
     * @return mixed
     */
    public function postUser(Request $request, Response $response, User $user) {

        /**
         * doing some basic validation BEFORE signup is executed
         */
        $validation = $this->validator->validate($request, [

            'Email'             => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'Password'          => v::noWhitespace()->notEmpty()->length(6, null),
            'Confirm-Password'  => v::noWhitespace()->notEmpty()->confirmPassword($request->getParam('Password'))
        ]);

        /**
         * if validation fails, then we redirect the user to : /BACK/USER/CATALOGUE
         */
        if ($validation->fails()) {

            return $response->withRedirect($this->router->pathFor('user.catalogue'));
        }

        $token = $this->uidGenerate->softwareLicense('12', '444', '', '');

        /**
         * if validation is acceptable, THEN signup is being executed
         */
        $user = User::with([])->create([

            'email'         => $request->getParam('Email'),
            'password'      => password_hash($request->getParam('Password'), PASSWORD_DEFAULT),
            'role'          => 1,
            'first_name'    => ucwords($request->getParam('First-Name')),
            'last_name'     => ucwords($request->getParam('Last-Name')),
            'token'         => $token,
            'isActivated'   => false
        ]);

        $newUser = User::with([])->orderBy('created_at', 'desc')->first();

        /**
         * after the user is created, we then go ahead and create his contacts (db relation)
         */
        if ($newUser) {

            UserContact::with([])->create([

                'user_id'       => $newUser->id,
                'tel_home'      => '...',
                'tel_mobile'    => '...',
                'tel_work'      => '...',
                'accepts_sms'   => false
            ]);
        }

        /**
         * Show a toast after signup is successfully finished
         */
        $this->flash->addMessage('success', 'Welcome to Element');

        /**
         * AND send a welcome-mail to the user, that we've just created the profile!...
         */
        $user        = new User;
        $user->name  = ucwords( $request->getParam('First-Name'));
        $user->email = $request->getParam('Email');
        $user->token = $token;

        $this->mailer->to($user->email, $user->name)->send(new Welcome($user, $this->translator));

        return $response->withRedirect($this->router->pathFor('back.sales'));
    }

    /**
     * @param Response $response
     * @param Locale $locale
     * @param User $user
     * @param UserStatus $userStatus
     *
     * @return Response
     */
    public function getUserAccount(Response $response, Locale $locale, User $user, UserStatus $userStatus) {

        $locales    = $locale->with([])->where('activated', '=', true)->get();

        $user       = $user->with([

            //... insert relations here

        ])->where('id', '=', $this->auth->user()->id)->first();
        $userStatus = $userStatus->with([])->where('id', '=', $this->auth->user()->status)->first();

        if (!$this->auth->user()->id) {

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }

        return $this->view->render($response, 'back/main/user/Profile.twig', [

            'locales'   => $locales,
            'user'      => $user,
            'userStatus'=> $userStatus,
        ]);
    }

    /**
     * @param Response $response
     * @param Locale $locale
     * @param User $user
     * @param UserStatus $userStatus
     *
     * @return Response
     */
    public function getUserInbox(Response $response, Locale $locale, User $user, UserStatus $userStatus) {

        $locales    = $locale->with([])->where('activated', '=', true)->get();

        $user = $user->with([

            //... insert relations here

        ])->where('id', '=', $this->auth->user()->id)->first();
        $userStatus = $userStatus->with([])->where('id', '=', $this->auth->user()->status)->first();

        if (!$this->auth->user()->id) {

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }

        return $this->view->render($response, 'back/main/user/Inbox.twig', [

            'locales'   => $locales,
            'user'      => $user,
            'userStatus'=> $userStatus,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     */
    public function postUserInbox(Request $request, Response $response, User $user) {

        dump("Email sent");
        die;
    }

    /**
     * @param Response $response
     *
     * @return Response
     */
    public function getUserActivation(Response $response) {

        $user = User::with([])->where('token', '=', $_SESSION['authToken'])->first();

        return $this->view->render($response, 'back/authentication/ActivateAccount.twig', [

            'user' => $user,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function activateUser(Request $request, Response $response) {

        /**
         * attempt to signin...
         */
        $activate = $this->auth->activate(

            $request->getParam('token')
        );

        /**
         * if signin FAILS, then we redirect back...
         */
        if (!$activate) {

            $this->flash->addMessage('danger', 'Hmm ?! Prøv igen...');

            return $response->withRedirect($this->router->pathFor('auth.activate.user-from-token'));
        }

        $this->flash->addMessage('success', 'velkommen');

        return $response->withRedirect($this->router->pathFor('back.sales'));
    }

    /**
     * @param Response $response
     *
     * @return mixed
     */
    public function resendActivationEmail(Response $response) {

        $token = $this->uidGenerate->softwareLicense('12', '444', '', '');

        if ($token) {

            $this->auth->user()->updateActivationToken($token);

            $user        = new User;
            $user->name  = $this->auth->user()->first_name ." ". $this->auth->user()->last_name;
            $user->email = $this->auth->user()->email;
            $user->token = $token;

            $this->mailer->to($user->email, $user->name)->send(new NewToken($user, $this->translator));

            $this->flash->addMessage('success', ''); // we pass an empty string in message, because we want to use our locales instead!

        } else {

            $this->flash->addMessage('danger', ''); // we pass an empty string in message, because we want to use our locales instead!

        }

        return $response->withRedirect($this->router->pathFor('auth.activate.user'));
    }
}