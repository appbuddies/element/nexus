<?php

namespace app\controllers;

use app\models\{
    data\User,
    data\UserAddresses,
    data\UserContact,
    mail\Welcome
};

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use Respect\Validation\Validator as v;

class FirebaseController extends BaseController {


    public function getFacebookSignin() {

        $this->fireAuth->facebookLogin();
    }

    public function postFacebookSignin() {

    }
}