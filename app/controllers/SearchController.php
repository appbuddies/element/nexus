<?php

namespace app\controllers;

use app\models\{
    data\User,
    upload\Image
};

use Illuminate\Pagination\LengthAwarePaginator;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};


class SearchController extends BaseController {

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return Response
     */
    public function getSearchResults(Request $request, Response $response) {

        $input = $request->getQueryParam('input');

        return $this->view->render($response, '/back/search/Results.twig', [

            'input' => $input,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function postSearchResults(Request $request, Response $response) {

        // ...
    }

}