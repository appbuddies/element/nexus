<?php

namespace app\controllers;

use app\models\{
    data\Locale,
    data\User,
    data\UserStatus
};

use Illuminate\Pagination\LengthAwarePaginator;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class HomeController extends BaseController {

    /**
     * @param Request $request
     * @param Response $response
     * @param Locale $locale
     *
     * @return Response
     */
    public function home(Request $request, Response $response, Locale $locale) {

        $locales    = $locale->with([])->where('activated', '=', true)->get();

        /* Paginator example
        $page       = $request->getParam('page', 1);
        $perPage    = $request->getParam('perPage', 3);

        $userPaginator  = new LengthAwarePaginator(
            array_slice($locales, ($page - 1) * $perPage, $perPage),
            count($locales),
            $perPage,
            $page,
            [
                'path'  => $request->getUri()->getPath(),
                'query' => $request->getParams()
            ]
        );
        */

        return $this->view->render($response, '/front/Home.twig', [

            'locales'   => $locales,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param Locale $locale
     * @param UserStatus $userStatus
     *
     * @return Response
     */
    public function sales(Request $request, Response $response, Locale $locale, UserStatus $userStatus) {

        $locales    = $locale->with([])->where('activated', '=', true)->get();
        $userStatus = $userStatus->with([])->where('id', '=', $this->auth->user()->status)->first();

        return $this->view->render($response, '/back/main/dashboards/Sales.twig', [

            'locales'   => $locales,
            'userStatus'=> $userStatus,
        ]);
    }

    /**
     * @param Response $response
     * @param Locale $locale
     * @param UserStatus $userStatus
     *
     * @return Response
     */
    public function analytics(Response $response, Locale $locale, UserStatus $userStatus) {

        $locales    = $locale->with([])->where('activated', '=', true)->get();
        $userStatus = $userStatus->with([])->where('id', '=', $this->auth->user()->status)->first();

        return $this->view->render($response, '/back/main/dashboards/Analytics.twig', [

            'locales'   => $locales,
            'userStatus'=> $userStatus,
        ]);
    }

    /**
     * @param Response $response
     *
     * @return mixed
     */
    public function backupDatabase(Response $response) {

        exec('mysqldump --host='.$this->config->get('db.mysql.host').' --port='.$this->config->get('db.mysql.port').' --user='.$this->config->get('db.mysql.user').' --password='.$this->config->get('db.mysql.password').' '.$this->config->get('db.mysql.database').' > ./storage/database/data_'.date("d-m-Y_H:i:s").'.sql');

        return $response->withRedirect($this->router->pathFor('back.sales'));
    }
}