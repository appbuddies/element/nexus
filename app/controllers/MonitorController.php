<?php

namespace app\controllers;

use app\models\{
    data\Locale,
    data\User,
    data\UserStatus
};

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class MonitorController extends BaseController {

    /**
     * @param Response $response
     * @param Locale $locale
     * @param UserStatus $userStatus
     *
     * @return Response
     */
    public function getMonitorEndpoints(Response $response, Locale $locale, UserStatus $userStatus) {

        $locales    = $locale->with([])->where('activated', '=', true)->get();
        $userStatus = $userStatus->with([])->where('id', '=', $this->auth->user()->status)->first();

        return $this->view->render($response, '/back/other/endpoints/Monitor.twig', [

            'locales'   => $locales,
            'userStatus'=> $userStatus,
        ]);
    }

    public function postMonitorEndpoints() {

        // TODO
    }

    /**
     * @param Response $response
     *
     * @return mixed
     */
    public function cronRunUptime(Response $response) {

        system('php element endpoint:run');

        return $response->withRedirect($this->router->pathFor('admin.back'));
    }
}
