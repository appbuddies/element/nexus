<?php

namespace app\controllers;

use app\handlers\auth\AuthSocial;

use app\models\data\{
    User,
    UserAddresses,
    UserContact,
    UserSocial
};

use app\models\mail\{
    PasswordReset,
    Welcome
};

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use Respect\Validation\Validator as v;

class AuthController extends BaseController {

    /**
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function getLogin(Response $response, User $user) {

        $users = $user->with([])->get();

        return $this->view->render($response, '/back/authentication/SignIn.twig', [

            'users' => $users,
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function postLogin(Request $request, Response $response, User $user) {

        /**
         * attempt to signin...
         */
        $auth = $this->auth->attempt(

            $request->getParam('email'),
            $request->getParam('password')
        );

        /**
         * if signin FAILS, then we redirect back...
         */
        if (!$auth) {

            $this->flash->addMessage('danger', 'Hmm ?! Try again...');
            return $this->view->render($response, '/back/authentication/SignIn.twig');
        }

        $user = $user->with([])->where('id', '=', $_SESSION['user'])->first();

        $userIsActivated = $user->isActivated;

        if ($userIsActivated) {

            $this->flash->addMessage('success', 'velkommen');

            return $response->withRedirect($this->router->pathFor('back.sales'));

        } else {

            /**
             * making sure to clear the _SESSION['user'] again,
             * if the user hasn't activated his account ?!
             */
            $this->auth->logout();

            /**
             * and instead we create a temporary key in session
             * with the users auth token, so we can validate him
             * in the nex step...
             */
            if ($user->token) {

                $_SESSION['authToken'] = $user->token;

                return $response->withRedirect($this->router->pathFor('auth.activate.user-from-token'));

            } else {

                /**
                 * if the token is empty or null, we should then
                 * create a new one here before proceeding with
                 * the validation...
                 */
                dump("Token not found");
                die;

            }
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     *
     * @return Response
     */
    public function getSignup(Request $request, Response $response, User $user) {

        return $this->view->render($response, '/back/authentication/SignUp.twig', [

            //...
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     *
     * @return mixed
     */
    public function postSignup(Request $request, Response $response, User $user) {

        /**
         * doing some basic validation BEFORE signup is executed
         */
        $validation = $this->validator->validate($request, [

            'First-Name'        => v::noWhitespace()->notEmpty(),
            'Last-Name'         => v::noWhitespace()->notEmpty(),
            'Email'             => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'Password'          => v::noWhitespace()->notEmpty()->length(6, null),
            'Confirm-Password'  => v::noWhitespace()->notEmpty()->confirmPassword($request->getParam('Password'))
        ]);

        /**
         * if validation fails, then we redirect the user to : XX ?!
         */
        if ($validation->fails()) {

            return $response->withRedirect($this->router->pathFor('back.signup'));
        }

        $token = $this->uidGenerate->softwareLicense('12', '444', '', '');

        /**
         * if validation is acceptable, THEN signup is being executed
         */
        $user = User::with([])->create([

            'email'         => $request->getParam('Email'),
            'password'      => password_hash($request->getParam('Password'), PASSWORD_DEFAULT),
            'role'          => 1,
            'first_name'    => ucwords($request->getParam('First-Name')),
            'last_name'     => ucwords($request->getParam('Last-Name')),
            'token'         => $token,
            'isActivated'   => false
        ]);

        $newUser = User::with([])->orderBy('created_at', 'desc')->first();

        /**
         * after the user is created, we then go ahead and create his contacts (db relation)
         */
        if ($newUser) {

            UserContact::with([])->create([

                'user_id'       => $newUser->id,
                'tel_home'      => '...',
                'tel_mobile'    => '...',
                'tel_work'      => '...',
                'accepts_sms'   => false
            ]);
        }

        /**
         * after signup is finished, we then sign the user in automatically
         */
        $this->flash->addMessage('success', 'Welcome to Element');
        $this->auth->attempt($user->email, $request->getParam('Password'));

        /**
         * AND send a welcome-mail to the user, that we've just created the profile!...
         */
        $user        = new User;
        $user->name  = ucwords( $request->getParam('First-Name'));
        $user->email = $request->getParam('Email');
        $user->token = $token;

        $this->mailer->to($user->email, $user->name)->send(new Welcome($user, $this->translator));

        return $response->withRedirect($this->router->pathFor('home'));
    }

    /**
     * @param Response $response
     * @param UserSocial $sso
     *
     * @return Response
     */
    public function getAccountLink(Response $response, UserSocial $sso) {

        $sso = $sso->with([])->where('id', '=', $_SESSION['sso'])->first();

        //dump($sso);

        return $this->view->render($response, '/back/authentication/LinkAccounts.twig', [

            'sso' => $sso
        ]);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param UserSocial $sso
     * @param AuthSocial $authSocial
     *
     * @return Response
     */
    public function postAccountLink(Request $request, Response $response, UserSocial $sso, AuthSocial $authSocial) {

        /**
         * attempt to signin...
         */
        $auth = $this->auth->attempt(

            $request->getParam('email'),
            $request->getParam('password')
        );

        /**
         * if signin FAILS, then we redirect back...
         */
        if (!$auth) {

            $this->flash->addMessage('danger', 'Hmm ?! Prøv igen...');

            return $this->view->render($response, '/back/authentication/LinkAccounts.twig');

        } else {

            $sso = $sso->with([])->where('id', '=', $_SESSION['sso'])->first();

            $sso->update([

                'user_id' => $this->auth->user()->id
            ]);

            $authSocial->clear();

            $this->flash->addMessage('success', 'accounts linked successfully!');

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }
    }

    /**
     * @param Response $response
     *
     * @return Response
     */
    public function getPasswordReset(Response $response) {

        return $this->view->render($response, '/back/authentication/ResetPassword.twig', [

            //...
        ]);
    }

    /**
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function postPasswordReset(Request $request, Response $response) {

        $userFromDatabase = User::with([])->where('email', '=', $_POST['email'])->first();

        if ($userFromDatabase) {

            /**
             * Generate a random password (format: aaaa-bbbb-cccc)
             */
            $password = $this->uidGenerate->softwareLicense('12', '444', '', '');

            /**
             * AND send a reset-password-mail to the user, that we've just found,
             * from the submitted param with the newly created password
             */
            $user           = new User;
            $user->name     = $userFromDatabase->first_name;
            $user->email    = $request->getParam('email');
            $user->password = $password;

            $this->mailer->to($user->email, $user->name)->send(new PasswordReset($user, $this->translator));

            $userFromDatabase::with([])->update([

                'password'  => password_hash($password, PASSWORD_DEFAULT)
            ]);

            /**
             * after reset is finished, we should show an alert or toast to the user, that suggests checking his email
             */
            $this->flash->addMessage('success', ''); // we pass an empty string in message, because we want to use our locales instead!

            /*
             * Finally we make sure to redirect the user off to an appropriate url
             */
            return $response->withRedirect($this->router->pathFor('back.signin'));

        } else {

            /**
             * tells the user that we couldn't find him from the email that was submitted
             */
            $this->flash->addMessage('danger', ''); // we pass an empty string in message, because we want to use our locales instead!

            /*
             * Finally we make sure to redirect the user off to an appropriate url
             */
            return $response->withRedirect($this->router->pathFor('back.reset-password'));

        }
    }

    /**
     * sign -> OUT
     *
     * @param Response $response
     *
     * @return Mixed
     */
    public function signout(Response $response) {

        $this->auth->logout();

        return $response->withStatus(302)->withHeader('Location', $_SESSION['currentRoute']);
    }

    /**
     * Lock System
     *
     * @param Response $response
     *
     * @return Mixed
     */
    public function getSystemLock(Response $response) {

        return $this->view->render($response, '/back/authentication/Lockscreen.twig', [

            //...
        ]);
    }

    /**
     * Lock System
     *
     * @param Response $response
     *
     * @return Mixed
     */
    public function postSystemLock(Request $request, Response $response) {

        /**
         * attempt to signin...
         */
        $auth = $this->auth->attempt(

            $request->getParam('email'),
            $request->getParam('password')
        );

        /**
         * if signin FAILS, then we redirect back...
         */
        if (!$auth) {

            return $this->view->render($response, '/back/authentication/Lockscreen.twig');
        }

        return $response->withRedirect($this->router->pathFor('back.sales'));
    }
}