<?php

namespace app\controllers;

use app\handlers\{
    auth\Auth,
    firebase\Authentication as FireAuth,
    mailer\Mailer
};

use app\validations\contracts\ValidatorInterface;

use Element\Promethium\Generate as UidGenerate;

use Illuminate\Translation\Translator;

use Interop\Container\ContainerInterface;

use Noodlehaus\Config;

use Slim\{
    Flash\Messages as Flash,
    Router,
    Views\Twig
};

abstract class BaseController {

    /**
     * BaseController dependencies
     */
    protected $auth;
    protected $config;
    protected $container;
    protected $fireAuth;
    protected $flash;
    protected $mailer;
    protected $router;
    protected $translator;
    protected $uidGenerate;
    protected $validator;
    protected $view;

    /**
     * BaseController constructor.
     *
     * @param Auth $auth
     * @param Config $config
     * @param ContainerInterface $container
     * @param FireAuth $fireAuth
     * @param Flash $flash
     * @param Mailer $mailer
     * @param Router $router
     * @param Translator $translator
     * @param UidGenerate $uidGenerate
     * @param ValidatorInterface $validator
     * @param Twig $view
     */
    public function __construct(Auth $auth, Config $config, ContainerInterface $container, FireAuth $fireAuth, Flash $flash, Mailer $mailer, Router $router, Translator $translator, UidGenerate $uidGenerate, ValidatorInterface $validator, Twig $view) {

        $this->auth = $auth;
        $this->config = $config;
        $this->container = $container;
        $this->fireAuth = $fireAuth;
        $this->flash = $flash;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->translator = $translator;
        $this->uidGenerate = $uidGenerate;
        $this->validator = $validator;
        $this->view = $view;
    }

    /**
     * @param $property
     *
     * @return mixed
     */
    public function __get($property) {

        if($this->container->{$property}) {

            return $this->container->{$property};

        } else {

            return null;

        }
    }
}
