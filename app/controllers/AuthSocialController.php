<?php

namespace app\controllers;

use app\handlers\auth\{
    AuthSocial,
    social\Facebook,
    social\Github,
    social\Google,
    social\Linkedin,
    social\Microsoft,
    social\Twitter
};

use app\models\{
    data\UserSocial
};

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class AuthSocialController extends BaseController {

    // region [O] FACEBOOK

    /**
     * @param Facebook $auth
     */
    public function getFacebookSignIn(Facebook $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    /**
     * @param Response $response
     * @param Facebook $auth
     * @param AuthSocial $sso
     *
     * @return void
     */
    public function getFacebookStatus(Response $response, Facebook $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {

            $this->flash->addMessage('danger', 'something went wrong?! Please try again...');
            return $response->withRedirect($this->router->pathFor('back.signin'));
        }

        $facebook = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'facebook',
            'email'     => $facebook->email

        ], [

            'service'   => 'facebook',
            'uid'       => $facebook->uid,
            'username'  => $facebook->username,
            'name'      => $facebook->name,
            'email'     => $facebook->email,
            'photo'     => $facebook->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id === null) {

            return $response->withRedirect($this->router->pathFor('back.signin.link-accounts'));

        } else {

            $_SESSION['user'] = $user->user_id;

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }
    }

    // endregion

    // region [O] GITHUB

    /**
     * @param Github $auth
     */
    public function getGithubSignIn(Github $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    /**
     * @param Response $response
     * @param Github $auth
     * @param AuthSocial $sso
     *
     * @return void
     */
    public function getGithubStatus(Response $response, Github $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {

            $this->flash->addMessage('danger', 'something went wrong?! Please try again...');
            return $response->withRedirect($this->router->pathFor('back.signin'));
        }

        $github = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'github',
            'email'     => $github->email

        ], [

            'service'   => 'github',
            'uid'       => $github->uid,
            'username'  => $github->username,
            'name'      => $github->name,
            'email'     => $github->email,
            'photo'     => $github->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id === null) {

            return $response->withRedirect($this->router->pathFor('back.signin.link-accounts'));

        } else {

            $_SESSION['user'] = $user->user_id;

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }
    }

    // endregion

    // region [O] GOOGLE

    /**
     * @param Google $auth
     */
    public function getGoogleSignIn(Google $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    /**
     * @param Response $response
     * @param Google $auth
     * @param AuthSocial $sso
     *
     * @return mixed
     */
    public function getGoogleStatus(Response $response, Google $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {

            $this->flash->addMessage('danger', 'something went wrong?! Please try again...');
            return $response->withRedirect($this->router->pathFor('back.signin'));
        }

        $google = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'google',
            'email'     => $google->email

        ], [

            'service'   => 'goggle',
            'uid'       => $google->uid,
            'username'  => $google->username,
            'name'      => $google->name,
            'email'     => $google->email,
            'photo'     => $google->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id === null) {

            return $response->withRedirect($this->router->pathFor('back.signin.link-accounts'));

        } else {

            $_SESSION['user'] = $user->user_id;

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }
    }

    // endregion

    // region [X] LinkedIn

    /**
     * @param Linkedin $auth
     */
    public function getLinkedinSignIn(Linkedin $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    /**
     * @param Response $response
     * @param Linkedin $auth
     * @param AuthSocial $sso
     *
     * @return mixed
     */
    public function getLinkedinStatus(Response $response, Linkedin $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {
            die();
        }

        $linkedin = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'linkedin',
            'email'     => $linkedin->email

        ], [

            'service'   => 'linkedin',
            'uid'       => $linkedin->uid,
            'username'  => $linkedin->username,
            'name'      => $linkedin->name,
            'email'     => $linkedin->email,
            'photo'     => $linkedin->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id === null) {

            return $response->withRedirect($this->router->pathFor('back.signin.link-accounts'));

        } else {

            $_SESSION['user'] = $user->user_id;

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }
    }

    // endregion

    // region [O] MICROSOFT

    /**
     * @param Microsoft $auth
     */
    public function getMicrosoftSignIn(Microsoft $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    /**
     * @param Response $response
     * @param Microsoft $auth
     * @param AuthSocial $sso
     *
     * @return void
     */
    public function getMicrosoftStatus(Response $response, Microsoft $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {

            $this->flash->addMessage('danger', 'something went wrong?! Please try again...');
            return $response->withRedirect($this->router->pathFor('back.signin'));
        }

        $microsoft = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'microsoft',
            'email'     => $microsoft->email

        ], [

            'service'   => 'microsoft',
            'uid'       => $microsoft->uid,
            'username'  => $microsoft->username,
            'name'      => $microsoft->name,
            'email'     => $microsoft->email,
            'photo'     => $microsoft->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id === null) {

            return $response->withRedirect($this->router->pathFor('back.signin.link-accounts'));

        } else {

            $_SESSION['user'] = $user->user_id;

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }
    }

    // endregion

    // region [X] TWITTER

    /**
     * @param Twitter $auth
     */
    public function getTwitterSignIn(Twitter $auth) {

        header('Location: ' . $auth->authorizeUrl());
    }

    /**
     * @param Response $response
     * @param Twitter $auth
     * @param AuthSocial $sso
     *
     * @return mixed
     */
    public function getTwitterStatus(Response $response, Twitter $auth, AuthSocial $sso) {

        if (!isset($_GET['code'])) {
            die();
        }

        $twitter = $auth->getUser($_GET['code']);

        $user = UserSocial::with([])->firstOrCreate([

            'service'   => 'twitter',
            'email'     => $twitter->email

        ], [

            'service'   => 'twitter',
            'uid'       => $twitter->uid,
            'username'  => $twitter->username,
            'name'      => $twitter->name,
            'email'     => $twitter->email,
            'photo'     => $twitter->photo,
        ]);

        $sso->store($user->service, $user->uid);

        if ($user->user_id === null) {

            return $response->withRedirect($this->router->pathFor('back.signin.link-accounts'));

        } else {

            $_SESSION['user'] = $user->user_id;

            return $response->withRedirect($this->router->pathFor('back.sales'));
        }
    }

    // endregion

}