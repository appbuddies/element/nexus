<?php

namespace app;

use DI\{
    ContainerBuilder,
    Bridge\Slim\App as DIBridge
};

class App extends DIBridge {

    protected function configureContainer(ContainerBuilder $builder) {

        /**
         * adding settings(s) :
         */
        $builder->addDefinitions([

            'settings.displayErrorDetails' => getenv("APP_MODE") == "development" ? true : false,
            'settings.addContentLengthHeader' => false,
            'settings.determineRouteBeforeAppMiddleware' => true,

            'settings.images' => [
                'cache' => [
                    'path' => base_path('storage/cache/images')
                ]
            ]

        ]);

        /**
         * adding container :
         */
        $builder->addDefinitions(__DIR__ . '/container.php');
    }
}