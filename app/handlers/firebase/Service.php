<?php

namespace app\handlers\firebase;

use GuzzleHttp\Client;
use Noodlehaus\Config;

//use Kreait\Firebase\Auth;

abstract class Service {

    //protected $auth;
    protected $client;
    protected $config;

    /**
     * Service constructor.
     * @param Client $client
     * @param Config $config
     */
    public function __construct(Client $client, Config $config) {

        //$this->auth = $auth;
        $this->client = $client;
        $this->config = $config;
    }

    //abstract public function getAuthorizeUrl();

//    public function authorizeUrl() {
//
//        return $this->getAuthorizeUrl();
//    }
}
