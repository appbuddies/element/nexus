<?php

namespace app\handlers\auth\social;

use GuzzleHttp\Exception\GuzzleException;

/*
 * |--------------------------------------------------------------------------------------------|
 * | Visit the official docs :                                                                  |
 * | https://developer.twitter.com/en/docs/authentication/guides/log-in-with-twitter            |
 * |--------------------------------------------------------------------------------------------|
 */
class Twitter extends Service {

    public function getAuthorizeUrl() {

        try {

//            return "https://api.twitter.com/oauth/request_token"
//                . "?response_type=code"
//                . "&client_id=" . $this->config->get('sso.tw.client_id')
//                . "&redirect_uri=" . $this->config->get('sso.tw.redirect_uri')
//                . "&scope=r_basicprofile%20r_emailaddress"
//                . "&state=" . bin2hex(random_bytes(16));

            return "https://api.twitter.com/oauth/authorize"
                . "?oauth_token=" . $this->config->get('sso.tw.access_token_key');
                //. "&oauth_token=Z6eEdO8MOmk394WozF5oKyuAv855l4Mlqo7hhlSLik";

        } catch (\Exception $e) {

            return $e;
        }
    }

    public function getUserByCode($code) {

        dump("så vi her");
        dump($code);
        die;

        $token = $this->getAccessTokenFromCode($code);

        return $this->normalizeUser($this->getUserByToken($token));
    }

    protected function getAccessTokenFromCode($code) {


//        return "https://api.twitter.com/oauth/authorize"
//            . "?oauth_token=code";


        $response = $this->client->request('GET', 'https://graph.facebook.com/v2.3/oauth/access_token', [
            'query' => [
                'client_id' => $this->config->get('sso.tw.client_id'),
                'client_secret' => $this->config->get('sso.tw.client_secret'),
                'redirect_uri' => $this->config->get('sso.tw.redirect_uri'),
                'code' => $code,
            ]
        ])->getBody();

        return json_decode($response)->access_token;
    }

    protected function getUserByToken($token) {

        $response = $this->client->request('GET', 'https://graph.facebook.com/me', [
            'query' => [
                'access_token' => $token,
                'fields' => 'id,name,email,picture'
            ],
        ])->getBody();

        return json_decode($response);
    }

    protected function normalizeUser($user) {

        return (object) [

            'uid'       => $user->id,
            //'username'  => $user->short_name,
            'name'      => $user->name,
            'email'     => $user->email,
            'photo'     => $user->picture->data->url,
        ];
    }
}
