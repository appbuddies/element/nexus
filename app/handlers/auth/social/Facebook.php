<?php

namespace app\handlers\auth\social;

use GuzzleHttp\Exception\GuzzleException;

/*
 * |--------------------------------------------------------------------------------------------|
 * | Visit the official docs :                                                                  |
 * | ?                                                                                          |
 * |--------------------------------------------------------------------------------------------|
 */
class Facebook extends Service {

    /**
     * @return string
     */
    public function getAuthorizeUrl() {

        try {

            return "https://www.facebook.com/dialog/oauth"
                . "?client_id=" . $this->config->get('sso.fb.app_id')
                . "&redirect_uri=" . $this->config->get('sso.fb.redirect_uri')
                . "&scope=email,public_profile"
                . "&state=" . bin2hex(random_bytes(16));

        } catch (\Exception $e) {

            return $e;
        }
    }

    public function getUserByCode($code) {

        $token = $this->getAccessTokenFromCode($code);

        return $this->normalizeUser($this->getUserByToken($token));
    }

    protected function getAccessTokenFromCode($code) {

        try {

            $response = $this->client->request('GET', 'https://graph.facebook.com/v2.3/oauth/access_token', [
                'query' => [
                    'client_id' => $this->config->get('sso.fb.app_id'),
                    'client_secret' => $this->config->get('sso.fb.app_secret'),
                    'redirect_uri' => $this->config->get('sso.fb.redirect_uri'),
                    'code' => $code,
                ]
            ])->getBody();

        } catch (GuzzleException $e) {

            dump($e);
            die;

        }

        return json_decode($response)->access_token;
    }

    protected function getUserByToken($token) {

        try {

            $response = $this->client->request('GET', 'https://graph.facebook.com/me', [
                'query' => [
                    'access_token' => $token,
                    'fields' => 'id,name,email,picture'
                ],
            ])->getBody();

        } catch (GuzzleException $e) {

            dump($e);
            die;

        }

        return json_decode($response);
    }

    protected function normalizeUser($user) {

        return (object) [

            'uid'       => $user->id,
            'username'  => $user->id,
            'name'      => $user->name,
            'email'     => $user->email,
            'photo'     => $user->picture->data->url,
        ];
    }
}
