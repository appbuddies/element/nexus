<?php

namespace app\handlers\auth\social;

use GuzzleHttp\Exception\GuzzleException;

/*
 * |--------------------------------------------------------------------------------------------|
 * | Visit the official docs :                                                                  |
 * | https://developers.google.com/identity/protocols/oauth2/openid-connect#createxsrftoken     |
 * |--------------------------------------------------------------------------------------------|
 */
class Google extends Service {

    public function getAuthorizeUrl() {

        try {

            return "https://accounts.google.com/o/oauth2/v2/auth"
                . "?response_type=code"
                . "&client_id=" . $this->config->get('sso.go.client_id')
                . "&scope=openid%20email"
                . "&redirect_uri=" . $this->config->get('sso.go.redirect_uri')
                . "&state=" . bin2hex(random_bytes(16))
                . "&nonce=" . bin2hex(random_bytes(16));

        } catch (\Exception $e) {

            return $e;
        }
    }

    public function getUserByCode($code) {

        $token = $this->getAccessTokenFromCode($code);

        return $this->normalizeUser($this->getUserByToken($token));
    }

    protected function getAccessTokenFromCode($code) {

        try {

            $response = $this->client->request('POST', 'https://oauth2.googleapis.com/token', [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                    'code'          => $code,
                    'client_id'     => $this->config->get('sso.go.client_id'),
                    'client_secret' => $this->config->get('sso.go.client_secret'),
                    'redirect_uri'  => $this->config->get('sso.go.redirect_uri'),
                    'grant_type'    => 'authorization_code'
                ]
            ])->getBody();

        } catch (GuzzleException $e) {

            dump($e);
            die;

        }

        return json_decode($response)->access_token;
    }

    protected function getUserByToken($token) {

        // TODO ->

        try {

            $response = $this->client->request('GET', 'https://openidconnect.googleapis.com/v1/userinfo', [
                'headers' => [
                    "Authorization" => "Bearer " . $token
                ]
            ])->getBody();

        } catch (GuzzleException $e) {

            dump($e);
            die;

        }

        return json_decode($response);
    }

    protected function normalizeUser($user) {

//        dump("user :");
//        dump($user);
//        die;

        return (object) [

            'uid'       => $user->sub,
            'username'  => null,
            'name'      => null,
            'email'     => $user->email,
            'photo'     => $user->picture,
        ];
    }
}
