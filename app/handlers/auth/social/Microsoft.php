<?php

namespace app\handlers\auth\social;

use GuzzleHttp\Exception\GuzzleException;

/*
 * |--------------------------------------------------------------------------------------------|
 * | Visit the official docs                                                                    |
 * | https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow   |
 * |--------------------------------------------------------------------------------------------|
 */
class Microsoft extends Service {

    public function getAuthorizeUrl() {

        try {

            return "https://login.microsoftonline.com/consumers/oauth2/v2.0/authorize"
                . "?client_id=" . $this->config->get('sso.ms.client_id')
                . "&response_type=code"
                . "&redirect_uri=" . $this->config->get('sso.ms.redirect_uri')
                . "&response_mode=query"
                . "&scope=User.Read User.ReadBasic.All openid"
                . "&state=" . bin2hex(random_bytes(16));

        } catch (\Exception $e) {

            return $e;
        }
    }

    public function getUserByCode($code) {

        $token = $this->getAccessTokenFromCode($code);

        return $this->normalizeUser($this->getUserByToken($token));
    }

    protected function getAccessTokenFromCode($code) {

        try {

            $response = $this->client->request('POST', 'https://login.microsoftonline.com/consumers/oauth2/v2.0/token', [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'form_params' => [
                    'client_id'     => $this->config->get('sso.ms.client_id'),
                    'client_secret' => $this->config->get('sso.ms.client_secret'),
                    'grant_type'    => 'authorization_code',
                    'code'          => $code
                ]
            ])->getBody();

        } catch (GuzzleException $e) {

            dump($e);
            die;

        }

        return json_decode($response)->access_token;
    }

    protected function getUserByToken($token) {

        try {

            $response = $this->client->request('GET', 'https://graph.microsoft.com/v1.0/me', [
                'headers' => [
                    "Authorization" => "Bearer " . $token
                ]
            ])->getBody();

        } catch (GuzzleException $e) {

            dump($e);
            die;

        }

        return json_decode($response);
    }

    protected function normalizeUser($user) {

        return (object) [

            'uid'       => $user->id,
            'name'      => $user->displayName,
            'email'     => $user->userPrincipalName,
            //'photo'     => $user->picture->data->url,
        ];
    }
}
