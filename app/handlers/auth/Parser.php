<?php

namespace app\handlers\auth;

use app\providers\jwt\JwtProviderInterface;

class Parser {

    protected $jwtProvider;

    /**
     * Parser constructor.
     *
     * @param JwtProviderInterface $jwtProvider
     */
    public function __construct(JwtProviderInterface $jwtProvider) {

        $this->jwtProvider = $jwtProvider;
    }

    /**
     * @param $token
     *
     * @return mixed
     */
    public function decode($token) {

        return $this->jwtProvider->decode(
            $this->extractToken($token)
        );
    }

    /**
     * @param $token
     *
     * @return null
     */
    protected function extractToken($token) {

        if (preg_match('/Bearer\s(\S+)/', $token, $matches)) {

            return $matches[1];
        }

        return null;
    }
}