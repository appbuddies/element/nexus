<?php

namespace app\handlers\auth;

use app\models\data\User;

class Auth {

	/**
	 * ...
	 */
	public function user() {

        if(isset($_SESSION['user'])) {

            return User::with([
                'role',
                'sso',
                'status'
            ])->find($_SESSION['user']);
        }

        return $_SESSION['user'] = null;
	}

	/**
	 * ...
	 */
	public function check() {

		return isset($_SESSION['user']);
	}

    /**
     * @param $email
     * @param $password
     *
     * @return bool
     */
	public function attempt($email, $password) {

		$user = User::with([])->where('email', '=', $email)->first();

		if(!$user) {

			return false;
		}

		if(password_verify($password, $user->password)) {

            $_SESSION['user'] = $user->id;

            $this->setOnlineStatus($_SESSION['user']);

            return true;
		}

		return false;
	}

    /**
     * @param $token
     *
     * @return bool
     */
    public function activate($token) {

        $user = User::with([])->where('token', '=', $token)->first();

        if(!$user) {

            return false;
        }

        if($token == $user->token) {

            User::with([])->where('id', '=', $user->id)->update([

                'token'         => null,
                'isActivated'   => 1
            ]);

            $_SESSION['user'] = $user->id;

            return true;
        }

        return false;
    }

	/**
	 * ...
	 */
	public function away() {

	    $this->setAwayStatus($_SESSION['user']);
	}

    /**
     * ...
     */
    public function logout() {

        $this->setOfflineStatus($_SESSION['user']);

        unset($_SESSION['user']);
    }

    /**
     * @param $id
     */
    private function setOfflineStatus($id) {

        User::with([])->where('id', '=', $id)->update([

            'status' => 1
        ]);
    }

    /**
     * @param $id
     */
    private function setOnlineStatus($id) {

	    User::with([])->where('id', '=', $id)->update([

	        'status' => 2
        ]);
    }

    /**
     * @param $id
     */
    private function setAwayStatus($id) {

        User::with([])->where('id', '=', $id)->update([

            'status' => 3
        ]);
    }
}