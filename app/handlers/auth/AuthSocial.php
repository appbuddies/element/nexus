<?php

namespace app\handlers\auth;

use app\models\data\UserSocial;

class AuthSocial {

	/**
	 * ...
	 */
	public function sso() {

        if(isset($_SESSION['sso'])) {

            return UserSocial::with([])->find($_SESSION['sso']);
        }

        return $_SESSION['sso'] = null;
	}

	/**
	 * ...
	 */
	public function check() {

		return isset($_SESSION['sso']);
	}

    /**
     * @param $service
     * @param $uid
     *
     * @return bool
     */
    public function store($service, $uid) {

        $sso = UserSocial::with([])->where('service', '=', $service)->where('uid', $uid)->first();

        if(!$sso) {

            return false;
        }

        if($sso->service) {

            $_SESSION['sso'] = $sso->id;
            return true;
        }

        return false;
    }

    /**
     * ...
     */
    public function clear() {

        unset($_SESSION['sso']);
    }
}