<?php

namespace app\handlers\errors;

use Psr\Http\Message\{
    ServerRequestInterface,
    ResponseInterface
};

use Slim\{
    Handlers\NotFound,
    Views\Twig
};

class NotFoundHandler extends NotFound {

	private $view;

    /**
     * NotFoundHandler constructor.
     *
     * @param Twig $view
     */
    public function __construct(Twig $view) {

		$this->view = $view;
	}

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response) {

		parent::__invoke($request, $response);

		$this->view->render($response, '/errors/404.twig', [

        ]);

		return $response->withStatus(404);
	}
}
