<?php

namespace app\handlers\app;

class AppVersion {

    public function getVersion() {

        $content = file_get_contents('composer.json');
        $composer = json_decode($content, true);

        return $composer['version'];
    }
}