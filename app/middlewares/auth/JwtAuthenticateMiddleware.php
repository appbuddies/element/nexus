<?php

namespace app\middlewares\auth;

use app\handlers\auth\JwtAuth;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

class JwtAuthenticateMiddleware {

    protected $auth;

    /**
     * JwtAuthenticateMiddleware constructor.
     * @param JwtAuth $auth
     */
    public function __construct(JwtAuth $auth) {

        $this->auth = $auth;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $next) {

        if (!$header = $this->getAuthorizationHeader($request)) {

            //return $response->withStatus(401);

            return $response->withJson([

                'message' => "Call to api failed : No JWT token was provided!?"
            ], 401);
        }

        try {

            $a = $this->auth->authenticate($header);
        }
        catch (Exception $e) {

            return $response->withJson([

                'message' => $e->getMessage()
            ], 401);
        }

        return $next($request, $response);
    }

    /**
     * @param Request $request
     * @return bool
     */
    protected function getAuthorizationHeader(Request $request) {

        if (!list($header) = $request->getHeader('Authorization', false)) {

            return false;
        }

        return $header;
    }
}