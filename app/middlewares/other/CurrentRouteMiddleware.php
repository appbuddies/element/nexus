<?php

namespace app\middlewares\other;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class CurrentRouteMiddleware {

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $next) {

        if ($response->getStatusCode() == 200) {

            /**
             * excluding certain routes from updating $_SESSION['currentRoute']
             */
            switch ($request->getUri('uri')->getPath()) {

                /**
                 * NOT allowed...
                 */
                case fnmatch('/user/wishlist/add/*', $request->getUri('uri')->getPath()):

                    break;

                case fnmatch('/back/inventory/product/*/edit/inventory-management', $request->getUri('uri')->getPath()):

                    break;

                case fnmatch('/back/inventory/product/*/edit/meta', $request->getUri('uri')->getPath()):

                    break;

                case fnmatch('/back/inventory/product/*/restock', $request->getUri('uri')->getPath()):

                    break;

                case fnmatch('/back/advertisement/add', $request->getUri('uri')->getPath()):

                    break;


                /**
                 * ALLOWED!
                 */
                default:

                    $currentRoute = $request->getUri('uri')->getPath();
                    $_SESSION['currentRoute'] = $currentRoute;
            }
        }

        $response = $next($request, $response);

        return $response;
    }
}