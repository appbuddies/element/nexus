<?php

namespace app\middlewares\other;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use Slim\Router;

class MaintenanceMiddleware {

    protected $router;

    public function __construct(Router $router) {

        $this->router = $router;
    }

    public function __invoke(Request $request, Response $response, $next) {

        if ($_SESSION['maintenance']) {

            return $response->withRedirect($this->router->pathFor('maintenance'));
        }

        $response = $next($request, $response);

        return $response;
    }
}