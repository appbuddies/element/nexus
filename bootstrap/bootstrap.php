<?php

/**
 * Pulling in vendor dependencies
 */
use app\App;
use app\middlewares\other\OldInputMiddleware;
use app\middlewares\ValidationErrorsMiddleware;
use Dotenv\Dotenv;
//use Kreait\Firebase\Factory as Firebase;
use Illuminate\Database\Capsule\Manager as Capsule;
use Noodlehaus\Config;
use Respect\Validation\Validator as v;
use Slim\Views\Twig;

/**
 * Start sessions
 */
session_start();

/**
 * Pulling in psr4 Autoload
 */
require __DIR__ . '/../vendor/autoload.php';

/**
 * Setting up : APP
 */
/* APP -> MODE */
try {

    (new Dotenv(__DIR__ . '/../', 'mode.env'))->load();
}
catch (Exception $e) {

    $e->getMessage();
}
/* APP */
$app = new App();
/* APP -> CONTAINER */
$container = $app->getContainer();
/* APP -> CONFIGS */
$config = $container->get(Config::class);

/**
 * setting up : CAPSULE
 */
$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => $config->get('db.mysql.driver'),
    'host'      => $config->get('db.mysql.host'),
    'port'      => $config->get('db.mysql.port'),
    'database'  => $config->get('db.mysql.database'),
    'username'  => $config->get('db.mysql.user'),
    'password'  => $config->get('db.mysql.password'),
    'charset'   => $config->get('db.mysql.charset'),
    'collation' => $config->get('db.mysql.collation'),
    'prefix'    => $config->get('db.mysql.prefix'),
]);
$capsule->setEventDispatcher(new Illuminate\Events\Dispatcher());
$capsule->setAsGlobal();
$capsule->bootEloquent();

/**
 * setting up : FIREBASE
 */
//$firebase = (new Firebase())->withServiceAccount($config->get('fb.config'));

/**
 * setting up : SESSION -> LANG
 */
/* default : LANG = DANISH */
if(!isset($_SESSION['lang'])) {
    $_SESSION['lang'] = $config->get('i18n.translations.fallback');
}

/**
 * dealing with : CORS
 */
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

/**
 * setting up : MIDDLEWARE
 */
/* -> OLD INPUT */
$app->add(new OldInputMiddleware($container->get(Twig::class)));
/* -> VALIDATION */
$app->add(new ValidationErrorsMiddleware($container->get(Twig::class)));

/**
 * Setting up : ROUTES
 */
/* -> API */
foreach (glob(__DIR__ . "/../src/routes/api/*.php") as $filename) {
    require_once $filename;
}
/* -> WWW */
foreach (glob(__DIR__ . "/../src/routes/www/*/*.php") as $filename) {
    require_once $filename;
}
/* -> i18n */
require_once __DIR__ . '/../src/routes/www/locale.php';

/**
 * setting up : VALIDATION RULES
 */
/* -> on orders */
v::with('app\\validations\\orders\\form\\');
/* -> on users */
v::with('app\\validations\\users\\domain\\');
v::with('app\\validations\\users\\email\\');
v::with('app\\validations\\users\\initials\\');
v::with('app\\validations\\users\\password\\');