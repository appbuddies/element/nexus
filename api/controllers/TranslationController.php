<?php

namespace api\controllers;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

use app\{
    controllers\BaseController
};

class TranslationController extends BaseController {

    /**
     * [X] GET CURRENT LOCALE
     *
     * @param Response $response
     *
     * @return mixed
     */
    public function getCurrentAdminLocales(Response $response) {

        $lang = $_SESSION['lang'];
        $locales = $this->translator->get('back', [], $lang);

        if ($locales) {

            try {

                return $response->withJson($locales, 200);

            } catch (\PDOException $e) {

                echo $e;
            }
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 404);
    }
}
