<?php

namespace api\controllers;

use app\{
    models\monitor\Endpoint
};

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class EndpointController extends BaseController {

    /**
     * [O] GET ALL ENDPOINTS
     *
     * @param Request $request
     * @param Response $response
     * @param Endpoint $endpoint
     *
     * @return mixed
     */
    public function getAllEndpoints(Request $request, Response $response, Endpoint $endpoint) {

        $status = $request->getParam('status');

        // TODO -> implement pagination functionality on weather to serve url's that are up or down only?!
        switch ($status) {

            case "up":

                dump("up");
                die;

                break;

            case "down":

                dump("down");
                die;

                break;

            default:

                $endpoints = $endpoint::with(['status', 'statuses'])->get();
        }

        if ($endpoints) {

            try {

                if (count($endpoints) == 0) {

                    $data['response'] = 'No endpoints are being monitored';
                    return $response->withJson($data, 200);

                } else {

                    return $response->withJson($endpoints, 200);

                }

            } catch (\PDOException $e) {

                echo $e;

                $data['response'] = 'API endpoint seems to be lost or moved?!';
                return $response->withJson($data, 404);
            }
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 400);
    }

}