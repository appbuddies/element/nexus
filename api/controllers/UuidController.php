<?php

namespace api\controllers;

use Element\Promethium\Exceptions\InvalidUidTypeException;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class UuidController extends BaseController {

    /**
     * [O] GENERATE AUTH-RECOVERY TOKENS
     *
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function generateAuthRecoveryTokens(Request $request, Response $response) {

        try {

            $tokens = $this->uidGenerate->authRecoveryTokens();

            return $response->withJson($tokens, 200);

        } catch (InvalidUidTypeException $e) {

            // ...do something
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }

    /**
     * [O] GENERATE OTP CODE
     *
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function generateOTP(Request $request, Response $response) {

        try {

            $code = $this->uidGenerate->otp();

            return $response->withJson($code, 200);

        } catch (InvalidUidTypeException $e) {

            // ...do something
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }

    /**
     * [O] GENERATE SOFTWARE LICENSE
     *
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function generateLicense(Request $request, Response $response) {

        $length     = (int) $request->getQueryParam('length');
        $format     = (int) $request->getQueryParam('format');
        $name       =       $request->getQueryParam('name');
        $software   =       $request->getQueryParam('software');

        try {

            $license = $this->uidGenerate->softwareLicense($length, $format, $name, $software);

            $data = (object) [
                "Software"      => $software,
                "License"       => $license,
                "Licensed to"   => $name
            ];

            return $response->withJson($data, 200);

        } catch (InvalidUidTypeException $e) {

            // ...do something
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }

    /**
     * [O] GENERATE TOKEN
     *
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function generateToken(Request $request, Response $response) {

        try {

            $token = $this->uidGenerate->token(PASSWORD_DEFAULT);

            return $response->withJson($token, 200);

        } catch (InvalidUidTypeException $e) {

            // ...do something
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }

    /**
     * [O] GENERATE UID
     *
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function generateUID(Request $request, Response $response) {

        try {

            $uid = $this->uidGenerate->uid('', false);

            return $response->withJson($uid, 200);

        } catch (InvalidUidTypeException $e) {

            // ...do something
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }
}