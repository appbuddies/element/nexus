<?php

namespace api\controllers;

use app\{
    models\data\User,
    models\data\UserContact,
    models\data\UserSocial,
    models\mail\Welcome
};

use Respect\Validation\Validator as v;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class UserController extends BaseController {

    /**
     * [O] GET USER FROM AUTH TOKEN (signin)
     *
     * @param Request $request
     * @param Response $response
     *
     * @return mixed
     */
    public function getUserFromAuthToken(Request $request, Response $response) {

        $authMethod = $request->getParam('authMethod');

        if ($authMethod === "2FA") {

            /**
             * ...Functionality pending...
             * (it doesn't do anything yet!)
             */
            $data['success'] = '2FA requested';
            return $response->withJson($data, 200);

        } else {

            try {

                $token = $request->getHeaderLine("Authorization");

                $authenticated = $this->jwt->authenticate($token);

                $user = $authenticated->user();

                $user = User::with([
                    'role',
                    'contacts',
                    //'addresses',
                    'sso',
                    //'services',
                    'status',
                    //'has_access'
                ])->where('id', '=', $user->id)->first();

                return $response->withJson($user, 200);

            } catch (\Exception $exception) {

                dump($exception);
                die;

            }
        }
    }

    /**
     * [O] GET ALL USERS
     *
     * @param Response $response
     * @param User $user
     *
     * @return mixed
     */
    public function getAllUsers(Response $response, User $user) {

        $users = $user::with([
            'contacts',
            'role',
            'sso',
            'status'
        ])->get();

        if ($users) {

            try {

                return $response->withJson($users, 200);

            } catch (\PDOException $e) {

                echo $e;
            }
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 404);
    }

    /**
     * [O] GET USER BY ID
     *
     * @param Response $response
     * @param User $user
     *
     * @return mixed
     */
    public function getUserByID(Response $response, User $user, $id) {

        $user = $user::with([
            'contacts',
            'role',
            'sso',
            'status'
        ])->where('id', '=', $id)->first();

        if ($user) {

            try {

                return $response->withJson($user, 200);

            } catch (\PDOException $e) {

                echo $e;
            }

        } else {

            $data['error'] = "Couldn't find a user with that ID";
            return $response->withJson($data, 404);

        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }

    /**
     * [O] EDIT USER
     *
     * @param Request $request
     * @param Response $response
     * @param $id
     * @param User $user
     * @param UserContact $contact
     *
     * @return mixed
     */
    public function updateUser(Request $request, Response $response, $id, User $user, UserContact $contact) {

        $user       = $user::with([])->where('id', '=', $id)->first();
        $contact    = $contact::with([])->where('user_id', '=', $id)->first();

        if ($user) {

            try {

                $user->update([
                    'uid'           => $request->getParam('uid'),
                    'email'         => $request->getParam('email'),
                    'password'      => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
                    'img_cover'     => $request->getParam('img_cover'),
                    'img_avatar'    => $request->getParam('img_avatar'),
                    'role'          => $request->getParam('role'),
                    'initials'      => strtolower($request->getParam('initials')),
                    'first_name'    => ucwords($request->getParam('first_name')),
                    'last_name'     => ucwords($request->getParam('last_name')),
                    'status'        => $request->getParam('status'),
                    'token'         => $request->getParam('token'),
                    'isActivated'   => $request->getParam('isActivated'),
                ]);

                if ($contact) {

                    $contact->update([

                        'tel_home'      => $request->getParam('tel_home'),
                        'tel_mobile'    => $request->getParam('tel_mobile'),
                        'tel_work'      => $request->getParam('tel_work'),
                        'accepts_sms'   => $request->getParam('accepts_sms'),
                    ]);
                }

                $data['error'] = 'User updated';
                return $response->withJson($data, 200);

            } catch (\PDOException $e) {

                echo $e;
            }

        } else {

            $data['error'] = "Couldn't find a user with that ID";
            return $response->withJson($data, 404);

        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }

    /**
     * [O] CREATE NEW USER
     *
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param UserContact $contact
     *
     * @return mixed
     */
    public function createUser(Request $request, Response $response, User $user, UserContact $contact) {

        /**
         * doing some basic validation BEFORE signup is executed
         */
        $validation = $this->validator->validate($request, [

            'first_name'        => v::noWhitespace()->notEmpty(),
            'last_name'         => v::noWhitespace()->notEmpty(),
            'email'             => v::noWhitespace()->notEmpty()->email()->emailAvailable(),
            'password'          => v::noWhitespace()->notEmpty()->length(6, null),
            'confirm_password'  => v::noWhitespace()->notEmpty()->confirmPassword($request->getParam('password'))
        ]);

        /**
         * if validation fails, then we redirect the user to : XX ?!
         */
        if ($validation->fails()) {

            $data['success'] = 'Validation failed';
            return $response->withJson($data, 400);
        }

        $token = $this->uidGenerate->softwareLicense('12', '444', '', '');

        try {

            // Creating the user
            $user = User::with([])->create([

                'uid'           => $request->getParam('uid'),
                'email'         => $request->getParam('email'),
                'password'      => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
                'img_cover'     => $request->getParam('img_cover'),
                'img_avatar'    => $request->getParam('img_avatar'),
                'role'          => $request->getParam('role'),
                'initials'      => strtolower($request->getParam('initials')),
                'first_name'    => ucwords($request->getParam('first_name')),
                'last_name'     => ucwords($request->getParam('last_name')),
                'status'        => 3,
                'token'         => $token,
                'isActivated'   => false,
            ]);

            // Getting the id from the last created user
            $newlyCreatedUser   = $user->with([])->orderBy('created_at', 'desc')->first();

            // Creating the user->contacts
            $contact = UserContact::with([])->create([

                'user_id'       => $newlyCreatedUser->id,
                'tel_home'      => $request->getParam('tel_home'),
                'tel_mobile'    => $request->getParam('tel_mobile'),
                'tel_work'      => $request->getParam('tel_work'),
                'accepts_sms'   => $request->getParam('accepts_sms'),
            ]);

            try {

                // AND sending a welcome-mail to the user, that we've just created!...
                $user        = new User;
                $user->name  = ucwords( $request->getParam('first_name'));
                $user->email = $request->getParam('email');
                $user->token = $token;

                $this->mailer->to($user->email, $user->name)->send(new Welcome($user, $this->translator));

            } catch (\Exception $e) {

                $data['error'] = $e;
                return $response->withJson($data, 404);
            }

            $data['success'] = 'User created';
            return $response->withJson($data, 200);

        } catch (\Exception $e) {

            $data['error'] = "Something went wrong";
            return $response->withJson($data, 500);
        }
    }

    /**
     * [O] DELETE USER
     *
     * @param Response $response
     * @param $id
     * @param User $user
     * @param UserContact $contact
     * @param UserSocial $userSocial
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function deleteUser(Response $response, $id, User $user, UserContact $contact, UserSocial $userSocial) {

        $user       = $user::with([])->where('id', '=', $id)->first();
        $contact    = $contact::with([])->where('user_id', '=', $id)->first();
        $sso        = $userSocial::with([])->where('user_id', '=', $id)->get();

        if ($user) {

            try {

                $user->delete();

                if ($contact) {

                    $contact->delete();
                }

                if ($sso) {

                    foreach ($sso as $social) {

                        $social->delete();
                    }
                }

                $data['success'] = 'User deleted from database';
                return $response->withJson($data, 200);

            } catch (\PDOException $e) {

                echo $e;
            }

        } else {

            $data['error'] = "Couldn't find a user with that ID";
            return $response->withJson($data, 404);

        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }
}