<?php

namespace api\controllers;

use app\handlers\{
    auth\JwtAuth,
    mailer\Mailer
};

use app\validations\contracts\ValidatorInterface;

use Element\Promethium\Generate as UidGenerate;

use Illuminate\Translation\Translator;

use Interop\Container\ContainerInterface;

use Noodlehaus\Config;

abstract class BaseController {

    /**
     * BaseController dependencies
     */
    protected $jwt;
    protected $config;
    protected $container;
    protected $mailer;
    protected $translator;
    protected $uidGenerate;
    protected $validator;

    /**
     * BaseController constructor.
     *
     * @param JwtAuth $jwt
     * @param Config $config
     * @param ContainerInterface $container
     * @param Mailer $mailer
     * @param Translator $translator
     * @param UidGenerate $uidGenerate
     * @param ValidatorInterface $validator
     */
    public function __construct(JwtAuth $jwt, Config $config, ContainerInterface $container, Mailer $mailer,Translator $translator, UidGenerate $uidGenerate, ValidatorInterface $validator) {

        $this->jwt = $jwt;
        $this->config = $config;
        $this->container = $container;
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->uidGenerate = $uidGenerate;
        $this->validator = $validator;
    }

    /**
     * @param $property
     *
     * @return mixed
     */
    public function __get($property) {

        if($this->container->{$property}) {

            return $this->container->{$property};

        } else {

            return null;

        }
    }
}
