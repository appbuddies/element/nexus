<?php

namespace api\controllers;

use app\{
    models\data\User,
    models\data\UserContact,
    models\data\UserSocial,
    models\mail\Welcome
};

use Respect\Validation\Validator as v;

use Psr\Http\Message\{
    ServerRequestInterface as Request,
    ResponseInterface as Response
};

class SessionController extends BaseController {

    /**
     * [O] GET SESSIONS
     *
     * @param Response $response
     *
     * @return mixed
     */
    public function getAllSessions(Response $response) {

        $sessions = $_SESSION;

        if ($sessions) {

            try {

                return $response->withJson($sessions, 200);

            } catch (\PDOException $e) {

                echo $e;
            }
        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 404);
    }

    /**
     * [O] GET SESSION FROM KEY
     *
     * @param Response $response
     * @param $key
     *
     * @return mixed
     */
    public function getSessionFromKey(Response $response, $key) {

        $session = $_SESSION[$key];

        if ($session) {

            try {

                return $response->withJson($session, 200);

            } catch (\PDOException $e) {

                echo $e;
            }

        } else {

            $data['error'] = "Couldn't find a session with that key";
            return $response->withJson($data, 404);

        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }

    /**
     * [O] DELETE SESSION KEY
     *
     * @param Response $response
     * @param $key
     *
     * @return mixed
     *
     */
    public function deleteSessionKey(Response $response, $key) {

        $session = $_SESSION[$key];

        if ($session) {

            try {

                // delete the key here...

                $data['success'] = 'Session key deleted';
                return $response->withJson($data, 200);

            } catch (\PDOException $e) {

                echo $e;
            }

        } else {

            $data['error'] = "Couldn't find a session-item with that key";
            return $response->withJson($data, 404);

        }

        $data['error'] = 'Something went wrong!?';
        return $response->withJson($data, 500);
    }
}