import axios from "axios"

export default {

    namespaced: true,

    state : {
        users: null
    },

    actions : {

        /**
         * Action(s) to fetch ALL users
         *
         * @param dispatch
         *
         * @returns {Promise<*>}
         */
        async fetchUsers({ commit }) {

            try {

                let response = await axios.get('/get/users')

                commit('saveUsers', response.data)

            } catch (e) {

                console.log("failed")

                commit("saveUsers", null)
            }
        }
    },

    getters : {

        users(state) {

            return state.users
        }
    },

    mutations : {

        saveUsers(state, payload) {
            state.users = payload
        }
    }
}
