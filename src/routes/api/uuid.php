<?php

use app\handlers\auth\JwtAuth;

use app\middlewares\auth\{
    JwtAuthenticateMiddleware
};

/**
 * OUTER Group that applies JWT to routes
 */
$app->group('/api', function () use($app, $container) {

    /**
     * GENERATE
     */
    $app->group('/generate', function () use($app, $container) {

        /**
         * ...Auth-Recovery Tokens
         */
        $this->get('/auth-recovery-tokens', ['api\controllers\UuidController', 'generateAuthRecoveryTokens'])->setName('api.generate.auth-recovery-tokens');

        /**
         * ...OTP
         */
        $this->get('/otp', ['api\controllers\UuidController', 'generateOTP'])->setName('api.generate.otp');

        /**
         * ...Software License
         */
        $this->get('/license', ['api\controllers\UuidController', 'generateLicense'])->setName('api.generate.license');

        /**
         * ...Token
         */
        $this->get('/token', ['api\controllers\UuidController', 'generateToken'])->setName('api.generate.token');

        /**
         * ...UID
         */
        $this->get('/uid', ['api\controllers\UuidController', 'generateUid'])->setName('api.generate.uid');

    });

})->add(new JwtAuthenticateMiddleware($container->get(JwtAuth::class)));