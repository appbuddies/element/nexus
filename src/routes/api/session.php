<?php

use app\handlers\auth\JwtAuth;

use app\middlewares\auth\{
    JwtAuthenticateMiddleware
};

/**
 * OUTER Group that applies JWT to routes
 */
$app->group('/api', function () use($app, $container) {

    /**
     * GET
     */
    $app->group('/get', function () use($app, $container) {

        /**
         * ...ALL SESSIONS
         */
        $this->get('/sessions', ['api\controllers\SessionController', 'getAllSessions'])->setName('api.get.sessions');

        /**
         * ...SESSION FROM KEY
         */
        $this->get('/session/{key}', ['api\controllers\SessionController', 'getSessionFromKey'])->setName('api.get.session.key');

    });

    /**
     * POST (create)
     */
//    $app->group('/new', function () use($app, $container) {
//
//        /**
//         * ...CREATE SESSION
//         */
//
//    });

    /**
     * PUT (update)
     */
//    $app->group('/edit', function () use($app, $container) {
//
//        /**
//         * ...EDIT SESSION
//         */
//
//    });

    /**
     * DELETE
     */
    $app->group('/delete', function () use($app, $container) {

        /**
         * ...DELETE USER
         */
        $this->delete('/session/{key}', ['api\controllers\SessionController', 'deleteSessionKey'])->setName('api.delete.session');

    });

})->add(new JwtAuthenticateMiddleware($container->get(JwtAuth::class)));