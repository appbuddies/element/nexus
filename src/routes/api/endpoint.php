<?php

use app\handlers\auth\JwtAuth;

use app\middlewares\auth\{
    JwtAuthenticateMiddleware
};

/**
 * OUTER Group that applies CSRF to routes
 */
$app->group('/api', function () use($app, $container) {

    /**
     * GET
     */
    $app->group('/get', function () use($app, $container) {

        /**
         * ...ALL ENDPOINTS
         */
        $this->get('/endpoints', ['api\controllers\EndpointController', 'getAllEndpoints'])->setName('api.get.endpoints');

    });

})->add(new JwtAuthenticateMiddleware($container->get(JwtAuth::class)));
