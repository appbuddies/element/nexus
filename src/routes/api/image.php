<?php

use app\handlers\auth\JwtAuth;

use app\middlewares\auth\{
    JwtAuthenticateMiddleware
};

/**
 * OUTER Group that applies JWT to routes
 */
$app->group('/api', function () use($app, $container) {

    /**
     * GET
     */
    $app->group('/get', function () use($app, $container) {

        /**
         * ...IMAGE BY UUID
         */
        $this->get('/image/{uuid}', ['api\controllers\ImageController', 'show'])->setName('api.get.image.id');

    });

    /**
     * POST
     */
    $app->group('/post', function () use($app, $container) {

        /**
         * ...IMAGES
         */
        $this->post('/images', ['api\controllers\ImageController', 'store'])->setName('api.post.images');

    });

})->add(new JwtAuthenticateMiddleware($container->get(JwtAuth::class)));