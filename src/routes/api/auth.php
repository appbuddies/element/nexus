<?php

/**
 * OUTER Group that DOESN'T applies CSRF to routes
 */
$app->group('/api', function () use($app, $container) {

    /**
     * POST (create)
     */
    $app->group('/auth', function () use($app, $container) {

        /**
         * retrieving : AUTH -> FETCH JWT TOKEN
         */
        $this->post('/fetch-token', ['api\controllers\AuthController', 'fetchToken'])->setName('api.fetch.token');

    });

});