<?php

use app\handlers\auth\JwtAuth;

use app\middlewares\auth\{
    JwtAuthenticateMiddleware
};

/**
 * OUTER Group that applies JWT to routes
 */
$app->group('/api', function () use($app, $container) {

    /**
     * GET
     */
    $app->group('/get', function () use($app, $container) {

        /**
         * ...USER from JWT token
         */
        $this->get('/user/me', ['api\controllers\UserController', 'getUserFromAuthToken'])->setName('api.get.user.from-auth-token');

        /**
         * ...ALL USERS
         */
        $this->get('/users', ['api\controllers\UserController', 'getAllUsers'])->setName('api.get.users');

        /**
         * ...USER BY ID
         */
        $this->get('/user/{id}', ['api\controllers\UserController', 'getUserByID'])->setName('api.get.user.id');

    });

    /**
     * POST (create)
     */
    $app->group('/new', function () use($app, $container) {

        /**
         * ...CREATE USER
         */
        $this->post('/user', ['api\controllers\UserController', 'createUser'])->setName('api.post.user');

    });

    /**
     * PUT (update)
     */
    $app->group('/edit', function () use($app, $container) {

        /**
         * ...EDIT USER
         */
        $this->put('/user/{id}', ['api\controllers\UserController', 'updateUser'])->setName('api.put.user');

    });

    /**
     * DELETE
     */
    $app->group('/delete', function () use($app, $container) {

        /**
         * ...DELETE USER
         */
        $this->delete('/user/{id}', ['api\controllers\UserController', 'deleteUser'])->setName('api.delete.user');

    });

})->add(new JwtAuthenticateMiddleware($container->get(JwtAuth::class)));