<?php

use app\handlers\auth\JwtAuth;

use app\middlewares\auth\{
    JwtAuthenticateMiddleware
};

/**
 * OUTER Group that applies CSRF to routes
 */
$app->group('/api', function () use($app, $container) {

    /**
     * GET
     */
    $app->group('/get', function () use($app, $container) {

        /**
         * ...ALL LOCALES FOR THE CURRENT LANG SELECTED
         */
        $this->get('/locale/admin', ['api\controllers\TranslationController', 'getCurrentAdminLocales'])->setName('api.get.locales.admin');

    });

})->add(new JwtAuthenticateMiddleware($container->get(JwtAuth::class)));
