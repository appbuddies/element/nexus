<?php

use Slim\Csrf\Guard;
use Slim\Views\Twig;

use app\middlewares\auth\CsrfViewMiddleware;

/**
 * OUTER Group that applies CSRF to routes
 */
$app->group('', function () use($app, $container) {

    /**
     * adding to view : LOCALIZATION
     */
    $app->get('/translate/{lang}', ['app\controllers\TranslationController', 'switch'])->setName('translate.switch');

})->add(new CsrfViewMiddleware($container->get(Twig::class), $container->get(Guard::class)))->add($container->get(Guard::class));
