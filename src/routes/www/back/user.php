<?php

use app\handlers\auth\Auth;

use app\middlewares\{
    auth\CsrfViewMiddleware,
    other\CurrentRouteMiddleware
};

use app\middlewares\back\{
    AuthMiddleware as AdminAuth,
    GuestMiddleware as AdminGuest
};

use Slim\{
    Csrf\Guard,
    Views\Twig
};


/**
 * OUTER Group that applies CSRF to routes
 */
$app->group('/back', function () use($app, $container) {

    /**
     * Group that DOES require the user to be signed in
     */
    $app->group('/activate', function () {

        /**
         * rendering view : USER -> ACCOUNT
         */
        //$this->get('/account', ['app\controllers\UserController', 'getUserActivation'])->setName('auth.activate.user');

        //$this->post('/account', ['app\controllers\UserController', 'activateUser'])->setName('auth.activate.user-token');
        //$this->post('/account/{email}', ['app\controllers\UserController', 'resendActivationEmail'])->setName('auth.resend.user-token');

    })->add(new AdminAuth($container->get(Auth::class), $container->get(\Slim\Router::class)));

    /**
     * Group that DOES require the user to be signed in
     */
    $app->group('/user', function () {

        /**
         * rendering view : USER -> ACCOUNT
         */
        $this->get('/account', ['app\controllers\UserController', 'getUserAccount'])->setName('user.account');

        /**
         * manage : USER -> ACCOUNT -> UPDATE ADDRESS'
         */
        //$this->post('/{id}/update-customer-address', ['app\controllers\UserController', 'postUserCustomerAddress'])->setName('user.update.customer-address');
        //$this->post('/{id}/update-shipping-address', ['app\controllers\UserController', 'postUserShippingAddress'])->setName('user.update.shipping-address');

        /**
         * manage : USER -> ACCOUNT -> UPDATE CONTACTS
         */
        //$this->post('/{id}/update-phone', ['app\controllers\UserController', 'postUserPhone'])->setName('user.update.phone');

        /**
         * manage : USER -> ACCOUNT -> UPDATE EMAIL
         */
        //$this->post('/{id}/update-email', ['app\controllers\UserController', 'postUserEmail'])->setName('user.update.email');

        /**
         * manage : USER -> ACCOUNT -> UPDATE PASSWORD
         */
        //$this->post('/{id}/update-password', ['app\controllers\UserController', 'postUserPassword'])->setName('user.update.password');

        /**
         * rendering view : USER -> INBOX
         */
        $this->get('/inbox', ['app\controllers\UserController', 'getUserInbox'])->setName('user.inbox');
        $this->post('/inbox', ['app\controllers\UserController', 'postUserInbox']);

        /**
         * rendering view : USER -> CATALOGUE
         */
        $this->get('/catalogue', ['app\controllers\UserController', 'getUsers'])->setName('user.catalogue');
        $this->post('/catalogue', ['app\controllers\UserController', 'postUser'])->setName('user.catalogue.add');

    })->add(new AdminAuth($container->get(Auth::class), $container->get(\Slim\Router::class)));

})
    ->add(new CsrfViewMiddleware($container->get(Twig::class), $container->get(Guard::class)))
    ->add($container->get(Guard::class))
    ->add(new CurrentRouteMiddleware())
;
