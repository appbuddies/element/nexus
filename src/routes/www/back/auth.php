<?php

use Slim\{
    Csrf\Guard,
    Views\Twig
};

use app\handlers\auth\Auth;

use app\middlewares\back\{
    AuthMiddleware as AdminAuth,
    GuestMiddleware as AdminGuest
};

use app\middlewares\front\{
    AuthMiddleware as HomeAuth,
    GuestMiddleware as HomeGuest
};

use app\middlewares\{
    auth\CsrfViewMiddleware,
    other\CurrentRouteMiddleware
};

/**
 * OUTER Group that applies CSRF to routes
 *
 * paths -> BACK
 */
$app->group('/back', function () use($app, $container) {

    /**
     * INNER Group that ??
     */
    $app->group('', function () {

        //...

    })->add(new HomeAuth($container->get(Auth::class), $container->get(\Slim\Router::class)));

    /**
     * INNER Group that DOESN'T allow the user to be signed in!...
     */
    $app->group('', function () use($app, $container) {

        /* auth -> SIGN-IN */
        $app->get('/login', [app\controllers\AuthController::class, 'getLogin'])->setName('back.signin');
        $app->post('/login', [app\controllers\AuthController::class, 'postLogin']);

        /* auth -> SIGN-IN -> WITH FIREBASE */
        $this->get('/login-with-firebase', ['app\controllers\FirebaseController', 'getFacebookSignin'])->setName('back.signin.with-firebase');
        $this->get('/login-with-firebase-status', ['app\controllers\FirebaseController', 'postFacebookSignin']);

        /* auth -> SIGN-IN -> WITH FACEBOOK */
        $this->get('/login-with-facebook', ['app\controllers\AuthSocialController', 'getFacebookSignin'])->setName('back.signin.with-facebook');
        $this->get('/login-with-facebook-status', ['app\controllers\AuthSocialController', 'getFacebookStatus']);

        /* auth -> SIGN-IN -> WITH GITHUB */
        $this->get('/login-with-github', ['app\controllers\AuthSocialController', 'getGithubSignIn'])->setName('back.signin.with-github');
        $this->get('/login-with-github-status', ['app\controllers\AuthSocialController', 'getGithubStatus']);

        /* auth -> SIGN-IN -> WITH GOOGLE */
        $this->get('/login-with-google', ['app\controllers\AuthSocialController', 'getGoogleSignIn'])->setName('back.signin.with-google');
        $this->get('/login-with-google-status', ['app\controllers\AuthSocialController', 'getGoogleStatus']);

        /* auth -> SIGN-IN -> WITH LINKEDIN */
        $this->get('/login-with-linkedin', ['app\controllers\AuthSocialController', 'getLinkedinSignIn'])->setName('back.signin.with-linkedin');
        $this->get('/login-with-linkedin-status', ['app\controllers\AuthSocialController', 'getLinkedinStatus']);

        /* auth -> SIGN-IN -> WITH MICROSOFT */
        $this->get('/login-with-microsoft', ['app\controllers\AuthSocialController', 'getMicrosoftSignIn'])->setName('back.signin.with-microsoft');
        $this->get('/login-with-microsoft-status', ['app\controllers\AuthSocialController', 'getMicrosoftStatus']);

        /* auth -> SIGN-IN -> WITH TWITTER */
        $this->get('/login-with-twitter', ['app\controllers\AuthSocialController', 'getTwitterSignin'])->setName('back.signin.with-twitter');
        $this->get('/login-with-twitter-status', ['app\controllers\AuthSocialController', 'getTwitterStatus']);

        /* auth -> SIGN-IN -> LINK ACCOUNTS */
        $this->get('/login/link-accounts', ['app\controllers\AuthController', 'getAccountLink'])->setName('back.signin.link-accounts');
        $this->post('/login/link-accounts', ['app\controllers\AuthController', 'postAccountLink']);

        /* auth -> RESET PASSWORD */
        $this->get('/reset-password', ['app\controllers\AuthController', 'getPasswordReset'])->setName('back.reset-password');
        $this->post('/reset-password', ['app\controllers\AuthController', 'postPasswordReset']);

        /* auth -> ACTIVATE ACCOUNT */
        $this->get('/activate/account', ['app\controllers\UserController', 'getUserActivation'])->setName('auth.activate.user-from-token');
        $this->post('/activate/account', ['app\controllers\UserController', 'activateUser']);

        /* auth -> RESEND ACTIVATION EMAIL */
        $this->post('/activate/account/{email}', ['app\controllers\UserController', 'resendActivationEmail'])->setName('auth.resend.user-token');

        /* auth -> SIGN-UP */
        $app->get('/signup', [app\controllers\AuthController::class, 'getSignup'])->setName('back.signup');
        $app->post('/signup', [app\controllers\AuthController::class, 'postSignup']);

    })->add(new AdminGuest($container->get(Auth::class), $container->get(\Slim\Router::class)));

    /**
     * INNER Group that DOES require the user to be signed in!...
     */
    $app->group('', function () use($app, $container) {

        /* auth -> LOCK SYSTEM */
        $app->get('/lock-system', [app\controllers\AuthController::class, 'getSystemLock'])->setName('back.lock-system');
        $app->post('/lock-system', [app\controllers\AuthController::class, 'postSystemLock']);

        /* auth -> SIGN-OUT */
        $app->get('/leave', [app\controllers\AuthController::class, 'signout'])->setName('back.signout');

    })->add(new AdminAuth($container->get(Auth::class), $container->get(\Slim\Router::class)));

})
    ->add(new CsrfViewMiddleware($container->get(Twig::class), $container->get(Guard::class)))
    ->add($container->get(Guard::class))
    ->add(new CurrentRouteMiddleware())
;