<?php

use app\handlers\auth\Auth;

use app\middlewares\{
    auth\CsrfViewMiddleware,
    other\CurrentRouteMiddleware
};

use app\middlewares\back\{
    AuthMiddleware as AdminAuth,
    GuestMiddleware as AdminGuest
};

use Slim\{
    Csrf\Guard,
    Views\Twig
};


/**
 * OUTER Group that applies CSRF to routes
 */
$app->group('/back', function () use($app, $container) {

    /**
     * Group that DOES require the user to be signed in
     */
    $app->group('/endpoint', function () {

        /**
         * rendering view : ENDPOINT -> MONITOR
         */
        $this->get('/monitor', ['app\controllers\MonitorController', 'getMonitorEndpoints'])->setName('back.monitor.endpoints');


    })->add(new AdminAuth($container->get(Auth::class), $container->get(\Slim\Router::class)));

})
    ->add(new CsrfViewMiddleware($container->get(Twig::class), $container->get(Guard::class)))
    ->add($container->get(Guard::class))
    ->add(new CurrentRouteMiddleware())
;
