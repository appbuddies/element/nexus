import Vue from 'vue'
import Router from 'vue-router'

// (^) is an alias for ./src/views
import Sales from '^/back/main/dashboards/Sales';
import Analytics from '^/back/main/dashboards/Analytics';
import Users from '^/back/other/users/Catalogue';
import Endpoints from '^/back/other/endpoints/Monitor';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/back/sales',
            name: 'back.sales',
            component: Sales
        },
        {
            path: '/back/analytics',
            name: 'back.analytics',
            component: Analytics
        },
        {
            path: '/back/user/catalogue',
            name: 'back.users.catalogue',
            component: Users
        },
        {
            path: '/back/endpoint/monitor',
            name: 'back.endpoint.monitor',
            component: Endpoints
        }
    ]
})