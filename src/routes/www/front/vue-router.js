import Vue from 'vue'
import Router from 'vue-router'

// (^) is an alias for ./src/views
import Home from '^/front/Home';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        }
    ]
})