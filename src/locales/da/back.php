<?php
/**
 * lang? = Danish (_da)
 */
return [

    /*--------------------------------------------------------------|
    |                                                               |
    |   VIEWS -> MAIN                                               |
    |                                                               |
    ---------------------------------------------------------------*/

    /**
     * app -> title & meta (_app)
     */
    /*--------------------------------------------------------------*/
    'app.admin_default_title'                   => "Nexus | Admin",
    'app.authentication_login'                  => "Nexus | Login",
    'app.authentication_login_reset_password'   => "Nexus | Nulstil adgangskode",
    'app.authentication_signup'                 => "Nexus | Ny bruger",
    'app.authentication_signup_activate_account'=> "Nexus | Aktivér bruger",
    'app.dashboard_sales'                       => "Nexus | Sales",
    'app.dashboard_analytics'                   => "Nexus | Analytics",
    'app.users_catalogue'                       => "Nexus | Brugere",
    /*--------------------------------------------------------------*/

    /**
     * app -> navbar -> item's (_nav)
     */
    /*--------------------------------------------------------------*/
    /* navitem : LOCALE */
    'nav.locale_title'                          => "Vælg Sprog",
    /*--------------------------------------------------------------*/
    /* navitem : LOCALE */
    'nav.user_profile_btn_1'                    => "Btn #1",
    'nav.user_profile_btn_2'                    => "Btn #2",
    'nav.user_profile_btn_3'                    => "Btn #3",
    'nav.user_profile_btn_profile'              => "Profil",
    'nav.user_profile_btn_sign_out'             => "Log ud",
    /*--------------------------------------------------------------*/

    /**
     * app -> sidebar (_side)
     */
    /*--------------------------------------------------------------*/
    'side.stat_offline'                         => "Offline",
    'side.stat_online'                          => "Online",
    'side.stat_away'                            => "Fraværende",
    'side.search'                               => "Søg...",
    /*--------------------------------------------------------------*/

    /**
     * app -> sidebar -> menu's (_menu)
     */
    /*--------------------------------------------------------------*/
    /* menu : SITE INFO */
    'menu.site_version'                         => "Version",
    /*--------------------------------------------------------------*/
    /* menu : MAIN */
    'menu.main_header_title'                    => "Primær",
    /* menu : MAIN -> KPI */
    'menu.main_kpi_title'                       => "KPI's",
    'menu.main_kpi_sales'                       => "Salg",
    'menu.main_kpi_analytics'                   => "Analyse",
    /* menu : MAIN -> USER */
    'menu.main_user_title'                      => "Bruger",
    'menu.main_user_profile'                    => "Profil",
    'menu.main_user_inbox'                      => "Indbakke",
    /*--------------------------------------------------------------*/
    /* menu : OTHER */
    'menu.other_header_title'                   => "Øvrige",
    /* menu : OTHER -> USERS */
    'menu.other_users_title'                    => "Brugere",
    /* menu : OTHER -> ENDPOINT MONITOR */
    'menu.other_endpoint_title'                 => "Endpoints",
    /* menu : OTHER -> SWAGGER UI */
    'menu.other_swagger_title'                  => "Swagger",
    /*--------------------------------------------------------------*/
    /* menu : AUTH */
    'menu.auth_header_title'                    => "Auth",
    /* menu : AUTH -> LOCK SYSTEM */
    'menu.auth_lock_system'                     => "Lås systemet",
    /* menu : AUTH -> SIGN-OUT */
    'menu.auth_sign_out'                        => "Log ud",
    /*--------------------------------------------------------------*/

    /**
     * app -> footer (_foot)
     */
    /*--------------------------------------------------------------*/
    'foot.copyright'                            => "Copyright",
    'foot.all_rights_reserved'                  => "Alle rettigheder forbeholdes.",
    'foot.cta_placeholder'                      => "Kan bruges til alt muligt",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MAIN -> KPI:SALES
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.kpi.sales.br_title'                   => "Salgsoversigt",
    'view.kpi.sales.br_desc'                    => "Valgfri beskrivelse",
    'view.kpi.sales.br_lvl_start'               => "nexus",
    'view.kpi.sales.br_lvl_main'                => "KPI",
    'view.kpi.sales.br_lvl_target'              => "Salg",
    /* area : SMALL BOXES */
    'view.kpi.sales.sm_01_title'                => "Nye Ordre",
    'view.kpi.sales.sm_02_title'                => "Bounce Rate",
    'view.kpi.sales.sm_03_title'                => "Brugere",
    'view.kpi.sales.sm_04_title'                => "Besøgende",
    'view.kpi.sales.sm_00_more_info_btn'        => "Mere info",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MAIN -> KPI:ANALYTICS
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.kpi.analytics.br_title'               => "Side Analyse",
    'view.kpi.analytics.br_desc'                => "Valgfri beskrivelse",
    'view.kpi.analytics.br_lvl_start'           => "nexus",
    'view.kpi.analytics.br_lvl_main'            => "KPI",
    'view.kpi.analytics.br_lvl_target'          => "Analyse",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MAIN -> USER:PROFILE
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.user.profile.br_title'                => "Brugerprofil",
    'view.user.profile.br_desc'                 => "Valgfri beskrivelse",
    'view.user.profile.br_lvl_start'            => "nexus",
    'view.user.profile.br_lvl_main'             => "Bruger",
    'view.user.profile.br_lvl_target'           => "Profil",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MAIN -> USER:INBOX
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.user.inbox.br_title'                  => "Indbakke",
    'view.user.inbox.br_desc'                   => "Valgfri beskrivelse",
    'view.user.inbox.br_lvl_start'              => "nexus",
    'view.user.inbox.br_lvl_main'               => "Bruger",
    'view.user.inbox.br_lvl_target'             => "Indbakke",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> OTHER -> USERS
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.other.users.br_title'                 => "Øvrige Brugere",
    'view.other.users.br_desc'                  => "Valgfri beskrivelse",
    'view.other.users.br_lvl_start'             => "nexus",
    'view.other.users.br_lvl_main'              => "Øvrige",
    'view.other.users.br_lvl_target'            => "Brugere",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MONITOR:ENDPOINTS
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.other.monitor.br_title'               => "Endpoints Monitor",
    'view.other.monitor.br_desc'                => "Valgfri beskrivelse",
    'view.other.monitor.br_lvl_start'           => "nexus",
    'view.other.monitor.br_lvl_main'            => "Øvrige",
    'view.other.monitor.br_lvl_target'          => "Endpoint Monitor",
    /*--------------------------------------------------------------*/

    /*--------------------------------------------------------------|
    |                                                               |
    |   VIEWS -> AUTH                                               |
    |                                                               |
    ---------------------------------------------------------------*/

    /**
     * app -> USER SIGN-IN (_app)
     */
    /*--------------------------------------------------------------*/
    'app.user_sign_in_headline'                 => "Log på systemet",
    'app.user_sign_in_email_placeholder'        => "Email",
    'app.user_sign_in_email_password'           => "Adgangskode",
    'app.user_sign_in_remember_me'              => "Husk mig",
    'app.user_sign_in_button'                   => "Login",
    'app.user_sign_in_with_socials_linebreak'   => "eller",
    'app.user_sign_in_with_socials_facebook'    => "Log på med facebook",
    'app.user_sign_in_with_socials_github'      => "Log på med Github",
    'app.user_sign_in_with_socials_google'      => "Log på med Google",
    'app.user_sign_in_with_socials_linkedin'    => "Log på med LinkedIn",
    'app.user_sign_in_with_socials_microsoft'   => "Log på med Microsoft",
    'app.user_sign_in_with_socials_twitter'     => "Log på med Twitter",
    'app.user_sign_in_forgot_password'          => "Glemt Adgangskoden?",
    'app.user_sign_in_create_new_user'          => "Opret ny bruger",
    /*--------------------------------------------------------------*/

    /**
     * app -> USER SIGN-IN -> FORGOT PASSWORD (_app)
     */
    /*--------------------------------------------------------------*/
    'app.user_forgot_pwd_headline'              => "Mistet din adgangskode!?",
    'app.user_forgot_pwd_password_placeholder'  => "Email",
    'app.user_forgot_pwd_paragraph'             => "Send os din email, så laver vi en ny adgangskode  til dig",
    'app.user_forgot_pwd_paragraph_hint'        => "(...bare husk at ændrer det igen, så snart du er logget ind!)",
    /*--------------------------------------------------------------*/

    /**
     * app -> USER SIGN-UP (_app)
     */
    /*--------------------------------------------------------------*/
    'app.user_sign_up_headline'                 => "Registrer ny bruger",
    'app.user_sign_up_first_name_placeholder'   => "Fornavn",
    'app.user_sign_up_last_name_placeholder'    => "Efternavn",
    'app.user_sign_up_email_placeholder'        => "Email",
    'app.user_sign_up_password_placeholder'     => "Adgangskode",
    'app.user_sign_up_password_placeholder_conf'=> "Bekræft adgangskoden",
    'app.user_sign_up_agree_to_terms_text'      => "Acceptér",
    'app.user_sign_up_agree_to_terms_link'      => "betingelserne",
    'app.user_sign_up_button'                   => "Opret",
    'app.user_sign_up_im_already_a_member'      => "Jeg har allerede en konto",
    /*--------------------------------------------------------------*/

    /**
     * app -> USER ACTIVATION (_app)
     */
    /*--------------------------------------------------------------*/
    'app.user_acct_activate_headline'           => "Angiv koden som du har fået",
    'app.user_acct_activate_paragraf'           => "tilsendt i velkomstmailen",
    'app.user_acct_activate_token'              => "token",
    'app.user_acct_activate_resend'             => "Send igen",
    'app.user_acct_activate_customer'           => "kundeservice",
    /*--------------------------------------------------------------*/


    /*--------------------------------------------------------------|
    |                                                               |
    |   COMPONENTS                                                  |
    |                                                               |
    ---------------------------------------------------------------*/

    /* item : DATATABLES */
    'components.datatable_decimal'              => "",
    'components.datatable_thousands'            => "",
    'components.datatable_empty_table'          => "",
    'components.datatable_search'               => "Søg:",
    'components.datatable_info'                 => "Viser side _PAGE_ af _PAGES_",
    'components.datatable_info_empty'           => "Ingen rækker tilgengængelige",
    'components.datatable_info_filtered'        => "(filtrede fra _MAX_ rækker totalt)",
    'components.datatable_info_post_fix'        => "",
    'components.datatable_length_menu'          => "Vis _MENU_ rækker per side",
    'components.datatable_loading_records'      => "Indlæser rækker...",
    'components.datatable_processing'           => "Behandler data...",
    'components.datatable_paginate_first'       => "Første",
    'components.datatable_paginate_last'        => "Sidste",
    'components.datatable_paginate_next'        => "Næste",
    'components.datatable_paginate_previous'    => "Forrige",
    'components.datatable_zero_records'         => "Kunne desværre ikke finde noget",

];
