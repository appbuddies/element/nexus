<?php
/**
 * lang? = Danish (_da)
 */
return [

    /*--------------------------------------------------|
    |                                                   |
    |   MAIL -> USER -> WELCOME                         |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * auth -> welcome new user (_auth) TODO
     */
    'auth.welcome_title'                => "",
    'auth.welcome_greet'                => "",
    'auth.welcome_heading'              => "",
    'auth.welcome_subtitle'             => "",
    'auth.welcome_instructions'         => "",
    'auth.welcome_cta_button'           => "",
    'auth.welcome_regards'              => "- Dette er en automatisk email",
    'auth.welcome_sender'               => "fra en robot 🤖 ved navn Bob",

    /*--------------------------------------------------|
    |                                                   |
    |   MAIL -> USER -> NEW TOKEN                       |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * auth -> new token (_auth)
     */
    'auth.new_token_title'              => "Her er din nye sikre nøgle 📣 🔐",
    'auth.new_token_greet'              => "Ohøj",
    'auth.new_token_heading'            => "Har du mistet aktiverings-nøglen?!...",
    'auth.new_token_dont_worry'         => "Bare rolig bro ✔️ 😄",
    'auth.new_token_generated'          => "Det er ikke noget problem, vi laver bare en ny til dig 👉",
    'auth.new_token_regards'            => "- Dette er en automatisk email",
    'auth.new_token_sender'             => "fra en robot 🤖 ved navn Bob",

    /*--------------------------------------------------|
    |                                                   |
    |   MAIL -> USER -> PASSWORD RESET                  |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * auth -> password reset (_auth)
     */
    'auth.password_reset_title'         => "Vi har nulstillet din adgangskode 📣 🔐",
    'auth.password_reset_greet'         => "Ohøj",
    'auth.password_reset_heading'       => "Har du mistet din adgangskode?!...",
    'auth.password_reset_dont_worry'    => "Bare rolig bro ✔️ 😄",
    'auth.password_reset_generated'     => "Vi har din ryg, så derfor har vi lavet en ny til dig 👉",
    'auth.password_reset_regards'       => "- Dette er en automatisk email",
    'auth.password_reset_sender'        => "fra en robot 🤖 ved navn Bob",
];
