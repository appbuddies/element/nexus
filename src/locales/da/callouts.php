<?php
/**
 * lang? = Danish (_da)
 */
return [

    /**
     * callout -> DEMO (_demo) // it's just for demo purposes!!
     */
    /*--------------------------------------------------------------*/
    'demo.success.title'        => "XX",
    'demo.success.message'      => "XX",
    'demo.info.title'           => "XX",
    'demo.info.message'         => "XX",
    'demo.warning.title'        => "XX",
    'demo.warning.message'      => "XX",
    'demo.danger.title'         => "XX",
    'demo.danger.message'       => "XX",
    /*--------------------------------------------------------------*/

    /**
     * callout -> PASSWORD RESET (_pwd-reset)
     */
    /*--------------------------------------------------------------*/
    'pwd-reset.success.title'   => "Nulstilling gennemført",
    'pwd-reset.success.message' => "...",
    'pwd-reset.success.btn-txt' => "Check din mail",
    'pwd-reset.danger.title'    => "Nulstilling fejlede",
    'pwd-reset.danger.message'  => "Noget gik galt?!",
    'pwd-reset.danger.btn-txt'  => "Forsøg venligst igen...",
    /*--------------------------------------------------------------*/

    /**
     * callout -> TOKEN GENERATED (_new-token)
     */
    /*--------------------------------------------------------------*/
    'new-token.success.title'   => "Ny token genereret",
    'new-token.success.message' => "...",
    'new-token.success.btn-txt'  => "Check din mail",
    'new-token.danger.title'    => "Generering af token fejlede",
    'new-token.danger.message'  => "Noget gik galt?!",
    'new-token.danger.btn-txt'  => "Forsøg venligst igen...",
    /*--------------------------------------------------------------*/

];
