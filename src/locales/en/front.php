<?php
/**
 * lang? = English (_en)
 */
return [

    /**
     * app -> title & meta (_app)
     */
    'app.home_title'        => "Element | Home",

    /**
     * app -> navbar (_nav)
     */
    'nav.link_dashboard'    => "Dashboard",
    'nav.link_login'        => "Login",
    'nav.link_register'     => "Register",
    'nav.link_signout'      => "Logout",

    /**
     * app -> links (_link)
     */
    'link.api'              => "Api",
    'link.docs'             => "Docs",
    'link.news'             => "News",
    'link.blog'             => "Blog",
    'link.gitlab'           => "Gitlab",
];
