<?php
/**
 * lang? = English (_en)
 */
return [

    /*--------------------------------------------------------------|
    |                                                               |
    |   VIEWS -> MAIN                                               |
    |                                                               |
    ---------------------------------------------------------------*/

    /**
     * app -> title & meta (_app)
     */
    /*--------------------------------------------------------------*/
    'app.admin_default_title'                   => "Nexus | Admin",
    'app.authentication_login'                  => "Nexus | Login",
    'app.authentication_login_reset_password'   => "Nexus | Reset password",
    'app.authentication_signup'                 => "Nexus | Signup",
    'app.authentication_signup_activate_account'=> "Nexus | Activate account",
    'app.dashboard_sales'                       => "Nexus | Sales",
    'app.dashboard_analytics'                   => "Nexus | Analytics",
    'app.users_catalogue'                       => "Nexus | Users",
    /*--------------------------------------------------------------*/

    /**
     * app -> navbar -> item's (_nav)
     */
    /*--------------------------------------------------------------*/
    /* navitem : LOCALE */
    'nav.locale_title'                          => "Set Language",
    /*--------------------------------------------------------------*/
    /* navitem : LOCALE */
    'nav.user_profile_btn_1'                    => "Btn #1",
    'nav.user_profile_btn_2'                    => "Btn #2",
    'nav.user_profile_btn_3'                    => "Btn #3",
    'nav.user_profile_btn_profile'              => "Profile",
    'nav.user_profile_btn_sign_out'             => "Sign out",
    /*--------------------------------------------------------------*/

    /**
     * app -> sidebar (_side)
     */
    /*--------------------------------------------------------------*/
    'side.stat_offline'                         => "Offline",
    'side.stat_online'                          => "Online",
    'side.stat_away'                            => "Away",
    'side.search'                               => "Search...",
    /*--------------------------------------------------------------*/

    /**
     * app -> sidebar -> menu's (_menu)
     */
    /*--------------------------------------------------------------*/
    /* menu : SITE INFO */
    'menu.site_version'                         => "Version",
    /*--------------------------------------------------------------*/
    /* menu : MAIN */
    'menu.main_header_title'                    => "Main",
    /* menu : MAIN -> KPI */
    'menu.main_kpi_title'                       => "KPI's",
    'menu.main_kpi_sales'                       => "Sale",
    'menu.main_kpi_analytics'                   => "Analytics",
    /* menu : MAIN -> USER */
    'menu.main_user_title'                      => "User",
    'menu.main_user_profile'                    => "Profile",
    'menu.main_user_inbox'                      => "Inbox",
    /*--------------------------------------------------------------*/
    /* menu : OTHER */
    'menu.other_header_title'                   => "Other",
    /* menu : OTHER -> USERS */
    'menu.other_users_title'                    => "users",
    /* menu : OTHER -> ENDPOINT MONITOR */
    'menu.other_endpoint_title'                 => "Endpoints",
    /* menu : OTHER -> SWAGGER UI */
    'menu.other_swagger_title'                  => "Swagger",
    /*--------------------------------------------------------------*/
    /* menu : AUTH */
    'menu.auth_header_title'                    => "Auth",
    /* menu : AUTH -> LOCK SYSTEM */
    'menu.auth_lock_system'                     => "Lock system",
    /* menu : AUTH -> SIGN-OUT */
    'menu.auth_sign_out'                        => "Sign out",
    /*--------------------------------------------------------------*/

    /**
     * app -> footer (_foot)
     */
    /*--------------------------------------------------------------*/
    'foot.copyright'                            => "Copyright",
    'foot.all_rights_reserved'                  => "All rights reserved.",
    'foot.cta_placeholder'                      => "Can be used for anything.",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MAIN -> KPI:SALES
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.kpi.sales.br_title'                   => "Sales overview",
    'view.kpi.sales.br_desc'                    => "Optional description",
    'view.kpi.sales.br_lvl_start'               => "nexus",
    'view.kpi.sales.br_lvl_main'                => "KPI",
    'view.kpi.sales.br_lvl_target'              => "Sale",
    /* area : SMALL BOXES */
    'view.kpi.sales.sm_01_title'                => "New Orders",
    'view.kpi.sales.sm_02_title'                => "Bounce Rate",
    'view.kpi.sales.sm_03_title'                => "Users",
    'view.kpi.sales.sm_04_title'                => "Unique Visitors",
    'view.kpi.sales.sm_00_more_info_btn'        => "More info",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MAIN -> KPI:ANALYTICS
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.kpi.analytics.br_title'               => "Page Analytics",
    'view.kpi.analytics.br_desc'                => "Optional description",
    'view.kpi.analytics.br_lvl_start'           => "nexus",
    'view.kpi.analytics.br_lvl_main'            => "KPI",
    'view.kpi.analytics.br_lvl_target'          => "Analytics",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MAIN -> USER:PROFILE
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.user.profile.br_title'                => "User Profile",
    'view.user.profile.br_desc'                 => "Optional description",
    'view.user.profile.br_lvl_start'            => "nexus",
    'view.user.profile.br_lvl_main'             => "User",
    'view.user.profile.br_lvl_target'           => "Profile",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MAIN -> USER:INBOX
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.user.inbox.br_title'                  => "User Inbox",
    'view.user.inbox.br_desc'                   => "Optional description",
    'view.user.inbox.br_lvl_start'              => "nexus",
    'view.user.inbox.br_lvl_main'               => "Bruger",
    'view.user.inbox.br_lvl_target'             => "Indbakke",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> OTHER -> USERS
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.other.users.br_title'                 => "Others Users",
    'view.other.users.br_desc'                  => "Optional description",
    'view.other.users.br_lvl_start'             => "nexus",
    'view.other.users.br_lvl_main'              => "Others",
    'view.other.users.br_lvl_target'            => "Brugere",
    /*--------------------------------------------------------------*/

    /**
     * app -> view -> MONITOR:ENDPOINTS
     */
    /*--------------------------------------------------------------*/
    /* area : BREADCRUMB */
    'view.other.monitor.br_title'               => "Endpoints Monitor",
    'view.other.monitor.br_desc'                => "Optional description",
    'view.other.monitor.br_lvl_start'           => "nexus",
    'view.other.monitor.br_lvl_main'            => "Others",
    'view.other.monitor.br_lvl_target'          => "Endpoint Monitor",
    /*--------------------------------------------------------------*/

    /*--------------------------------------------------------------|
    |                                                               |
    |   VIEWS -> AUTH                                               |
    |                                                               |
    ---------------------------------------------------------------*/

    /**
     * app -> USER SIGN-IN (_app)
     */
    /*--------------------------------------------------------------*/
    'app.user_sign_in_headline'                 => "Sign in to start your session",
    'app.user_sign_in_email_placeholder'        => "Email",
    'app.user_sign_in_password_placeholder'     => "Adgangskode",
    'app.user_sign_in_remember_me'              => "Remember me",
    'app.user_sign_in_button'                   => "Signin",
    'app.user_sign_in_with_socials_linebreak'   => "or",
    'app.user_sign_in_with_socials_facebook'    => "Sign in with facebook",
    'app.user_sign_in_with_socials_github'      => "Sign in with Github",
    'app.user_sign_in_with_socials_google'      => "Sign in with Google",
    'app.user_sign_in_with_socials_linkedin'    => "Sign in with LinkedIn",
    'app.user_sign_in_with_socials_microsoft'   => "Sign in with Microsoft",
    'app.user_sign_in_forgot_password'          => "Forgot password?",
    'app.user_sign_in_create_new_user'          => "Create new user",
    /*--------------------------------------------------------------*/

    /**
     * app -> USER SIGN-IN -> FORGOT PASSWORD (_app)
     */
    /*--------------------------------------------------------------*/
    'app.user_forgot_pwd_headline'              => "Lost you password ey'!?",
    'app.user_forgot_pwd_password_placeholder'  => "Email",
    'app.user_forgot_pwd_paragraph'             => "Let us know your email, and we'll send you a new one",
    'app.user_forgot_pwd_paragraph_hint'        => "(...just don't forget to change it, once your signed in!)",
    /*--------------------------------------------------------------*/

    /**
     * app -> USER SIGN-UP (_app)
     */
    /*--------------------------------------------------------------*/
    'app.user_sign_up_headline'                 => "Register new user",
    'app.user_sign_up_first_name_placeholder'   => "First name",
    'app.user_sign_up_last_name_placeholder'    => "Last name",
    'app.user_sign_up_email_placeholder'        => "Email",
    'app.user_sign_up_password_placeholder'     => "Password",
    'app.user_sign_up_password_placeholder_conf'=> "Confirm password",
    'app.user_sign_up_agree_to_terms_text'      => "Accept",
    'app.user_sign_up_agree_to_terms_link'      => "terms",
    'app.user_sign_up_button'                   => "Register",
    'app.user_sign_up_im_already_a_member'      => "I'm already a member",
    /*--------------------------------------------------------------*/

    /**
     * app -> USER ACTIVATION (_app)
     */
    /*--------------------------------------------------------------*/
    'app.user_acct_activate_headline'           => "Angiv koden som du har fået",
    'app.user_acct_activate_paragraf'           => "tilsendt i velkomstmailen",
    'app.user_acct_activate_token'              => "token",
    'app.user_acct_activate_resend'             => "Send igen",
    'app.user_acct_activate_customer'           => "kundeservice",
    /*--------------------------------------------------------------*/

];
