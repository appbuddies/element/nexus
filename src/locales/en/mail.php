<?php
/**
 * lang? = English (_en)
 */
return [

    /*--------------------------------------------------|
    |                                                   |
    |   MAIL -> USER -> WELCOME                         |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * auth -> welcome new user (_auth) TODO
     */
    'auth.welcome_title'                => "",
    'auth.welcome_greet'                => "",
    'auth.welcome_heading'              => "",
    'auth.welcome_subtitle'             => "",
    'auth.welcome_instructions'         => "",
    'auth.welcome_cta_button'           => "",
    'auth.welcome_regards'              => "- This is an automated response",
    'auth.welcome_sender'               => "from an Element 🤖 named Bob",

    /*--------------------------------------------------|
    |                                                   |
    |   MAIL -> USER -> NEW TOKEN                       |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * auth -> new token (_auth)
     */
    'auth.new_token_title'              => "Here is your new token 📣 🔐",
    'auth.new_token_greet'              => "Hi",
    'auth.new_token_heading'            => "Lost your activation-token hey?!...",
    'auth.new_token_dont_worry'         => "Don't worry mate ✔️ 😄",
    'auth.new_token_generated'          => "We've gone ahead and made you a new one 👉",
    'auth.new_token_regards'            => "- This is an automated response",
    'auth.new_token_sender'             => "from an Element 🤖 named Bob",

    /*--------------------------------------------------|
    |                                                   |
    |   MAIL -> USER -> PASSWORD RESET                  |
    |                                                   |
    ---------------------------------------------------*/

    /**
     * auth -> password reset (_auth)
     */
    'auth.password_reset_title'         => "Your password has been reset 📣 🔐",
    'auth.password_reset_greet'         => "Hi",
    'auth.password_reset_heading'       => "Forgot your password did you?!...",
    'auth.password_reset_dont_worry'    => "Don't worry mate ✔️ 😄",
    'auth.password_reset_generated'     => "We've gone ahead and made you a new one 👉",
    'auth.password_reset_regards'       => "- This is an automated response",
    'auth.password_reset_sender'        => "from an Element 🤖 named Bob",
];
