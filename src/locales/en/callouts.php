<?php
/**
 * lang? = English (_en)
 */
return [

    /**
     * callout -> DEMO (_demo) // it's just for demo purposes!!
     */
    /*--------------------------------------------------------------*/
    'demo.success.title'        => "YY",
    'demo.success.message'      => "YY",
    'demo.info.title'           => "YY",
    'demo.info.message'         => "YY",
    'demo.warning.title'        => "YY",
    'demo.warning.message'      => "YY",
    'demo.danger.title'         => "YY",
    'demo.danger.message'       => "YY",
    /*--------------------------------------------------------------*/

    /**
     * callout -> PASSWORD RESET (_pwd-reset)
     */
    /*--------------------------------------------------------------*/
    'pwd-reset.success.title'   => "Reset successfull",
    'pwd-reset.success.message' => "...",
    'pwd-reset.success.btn-txt' => "Check your email",
    'pwd-reset.danger.title'    => "Reset failed",
    'pwd-reset.danger.message'  => "Something didn't go right?!",
    'pwd-reset.danger.btn-txt'  => "please try again...",
    /*--------------------------------------------------------------*/

    /**
     * callout -> TOKEN GENERATED (_new-token)
     */
    /*--------------------------------------------------------------*/
    'new-token.success.title'   => "New token generated",
    'new-token.success.message' => "...",
    'new-token.success.btn-txt'  => "Check your email",
    'new-token.danger.title'    => "Token generating failed",
    'new-token.danger.message'  => "Something didn't go right?!",
    'new-token.danger.btn-txt'  => "please try again...",
    /*--------------------------------------------------------------*/

];
