import Vue from 'vue';
import App from '^/FrontView';
import router from '#/www/front/vue-router';
import store from '&/store';
import axios from "axios";
import config from "config/vue/configurator";
import regeneratorRuntime from "regenerator-runtime";

require("&/subscriber")

Vue.config.productionTip = config['app']['productionTip']

axios.defaults.baseURL = "/api"

store.dispatch('auth/attempt', localStorage.getItem('token')).then(() => {

    new Vue({
        el: '#app',
        router,
        config,
        store,
        regeneratorRuntime,
        render: h => h(App)
    });

})

// new Vue({
//     el: '#app',
//     router,
//     config,
//     store,
//     render: h => h(App)
// });