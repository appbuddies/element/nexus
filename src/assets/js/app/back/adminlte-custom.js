/**
 * custom jQuery (feel free to add your own :-D )
 */
$( document ).ready(function() {

    /**
     * change font-awesome icons onClick
     */
    $('#backupDB').click(function(){
        $(this).next('ul').slideToggle('500');
        $(this).find('i').toggleClass('fa fa-database fa fa-check')
    });

    /**
     * function : FULLSCREEN TOGGLE
     */
    (function ($) {
        'use strict';

        function toggleFullscreen(elem) {
            elem = elem || document.documentElement;
            if (
                !document.fullscreenElement &&
                !document.mozFullScreenElement &&
                !document.webkitFullscreenElement &&
                !document.msFullscreenElement
            ) {
                if (elem.requestFullscreen) {
                    elem.requestFullscreen();
                } else if (elem.msRequestFullscreen) {
                    elem.msRequestFullscreen();
                } else if (elem.mozRequestFullScreen) {
                    elem.mozRequestFullScreen();
                } else if (elem.webkitRequestFullscreen) {
                    elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                }
            } else {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }
        }

        $('#toggleFullscreen').on('click', function () {
            toggleFullscreen();
        });

    })(jQuery);

    /**
     * Custom "F9"-key shortcut function
     */
    document.onkeyup = function(key) {

        if (key.which === 120) {

            alert("Do something")
        }

    };

    /**
     * function : LOCALE OVERLAY TOGGLE
     */
    document.getElementById("openLocaleNavItem").onclick = function() {

        toggleLocaleOverlay(true)
    };

    document.getElementById("closeLocaleNavItem").onclick = function() {

        toggleLocaleOverlay(false)
    };

    function toggleLocaleOverlay(opens) {

        /**
         *
         * USAGE :
         *
         * Change width to height, so that the menu comes
         * down, instead of in from the left
         */
        if (opens) {

            document.getElementById("navItem_language").style.width = "100%";

        } else {

            document.getElementById("navItem_language").style.width = "0%";

        }
    }

});