const base = require('./webpack.base');
const merge = require ('webpack-merge');

const path = require('path');

const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

//const config = require('../../config/vue/development/app');
//const config = require('config/vue/configurator');

//console.log(config)

module.exports = merge(base, {

    devtool: 'source-map',

    mode: 'development',

    module: {
        rules: [

            // ...add your rules here!

        ]
    },

    plugins: [

        new webpack.DefinePlugin({
            'process': {
                //env: config
            }
        }),

        new BrowserSyncPlugin({

            /**
             *  enable (or disable) HTTPS
             *  [default = false]
             */
            https: false,

            /**
             *  ...
             *  [default = n/a]
             */
            host: 'localhost',

            /**
             *  port to be used for serving the application
             *  [default = n/a]
             */
            port: 3000,

            /**
             *  determine, if the notification in the upper right corner should be showed
             *  [default = true]
             */
            notify: true,

            /**
             *  serve files from this folder
             *  [default = n/a]
             */
            server: {
                baseDir: [
                    'public'
                ],
                index: "welcome.html",
                serveStaticOptions: {
                    extensions: [
                        "html"
                    ]
                },
                routes: {
                    // "/bower_components": "bower_components"
                }
            },

            /**
             *  watch for changes in these files
             *  [default = n/a]
             */
            // files: [
            //     {
            //         match: [
            //             '**/*.js',
            //             //'**/*.php',
            //             //'**/*.twig'
            //         ],
            //         files: [
            //             //'app/*.php',
            //             //'app/**/*.php',
            //             'app/*.js',
            //             'app/**/*.js',
            //         ],
            //         fn: function(event, file) {
            //             if (event === "change") {
            //                 const bs = require('browser-sync').get('bs-webpack-plugin');
            //                 bs.reload();
            //             }
            //         }
            //     }
            // ],

            /**
             *  serve BrowserSync UI on this port
             *  [default = n/a]
             */
            ui: {
                port: 8080
            }
        })

    ],

    watch: false

});
