const path = require('path');

const webpack = require('webpack');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyStaticsPlugin = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {

    devtool: 'source-map',

    mode: 'none',

    entry: {

        'api/app'                   : './src/assets/js/api/app.js',

        'back/adminlte'             : './src/assets/js/app/back/adminlte.js',
        'back/adminlte-custom'      : './src/assets/js/app/back/adminlte-custom.js',
        'back/vendors/icheck'       : './src/assets/js/app/back/vendors/iCheck/icheck.js',
        'back/vue-main'             : './src/assets/js/vue/main-back.js',

        'front/app'                 : './src/assets/js/app/front/app.js',
        'front/custom'              : './src/assets/js/app/front/custom.js',
        'front/vue-main'            : './src/assets/js/vue/main-front.js',

        'service-worker'            : './src/build/static/service-worker.js',
    },

    output: {
        filename: 'js/[name].min.js',
        path: path.resolve(__dirname, '../../public')
    },

    resolve: {
        alias: {
            '@': path.resolve('./src'),
            '^': path.resolve('./src/views'),
            '~': path.resolve('./src/components'),
            '#': path.resolve('./src/routes'),
            '&': path.resolve('./src/store'),
            'fonts': path.resolve('./src/assets/fonts'),
            'icons': path.resolve('./src/assets/icons'),
            'images': path.resolve('./src/assets/home'),
            'js': path.resolve('./src/assets/js'),
            'scss': path.resolve('./src/assets/scss'),
            'public': path.resolve('./public'),
            'config': path.resolve('./config'),
        },
        extensions: [
            '.js',
            '.vue',
            '.scss',
            '.twig'
        ]
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader'
                }]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: true // TODO
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                })
            },
            // {
            //     test: /\.(png|jpg|gif|svg|txt)$/,
            //     use: [
            //         {
            //             loader: 'file-loader',
            //             options: {
            //                 name(robots) {
            //                     return '[path][name].[ext]';
            //                 },
            //             },
            //         },
            //     ],
            // },
        ]
    },

    plugins: [

        new ExtractTextPlugin({
            filename:  (getPath) => {
                return getPath('css/[name].min.css').replace('css', 'css');
            },
            allChunks: true
        }),

        new OptimizeCSSPlugin({
            cssProcessorOptions: {
                map: {
                    inline: false
                },
                discardComments: {
                    removeAll: false
                }
            }
        }),

        new HtmlWebpackPlugin({
            filename: 'welcome.html',
            template: path.join(__dirname, './static/welcome.html'),
            files: {
                "css": [
                    "css/[name].min.css"
                ],
                "js": [
                    "js/[name].min.js"
                ],
            }
        }),

        new CopyStaticsPlugin({
            patterns: [
                {
                    from: path.join(__dirname, './static/browserconfig.xml'),
                    to: '.'
                },
                {
                    from: path.join(__dirname, './static/manifest.json'),
                    to: '.'
                },
                {
                    from: path.join(__dirname, './static/robots.txt'),
                    to: '.'
                },
                {
                    from: path.join(__dirname, './static/sitemap.xml'),
                    to: '.'
                }
            ]
        }),

        new VueLoaderPlugin(),

        new WebpackNotifierPlugin({
            contentImage: path.join(__dirname, './static/icons/element.png'),
            title: 'Elements collected',
            sound: true, // true | false.
        }),

    ]

};
