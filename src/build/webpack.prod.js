const base = require('./webpack.base');
const merge = require ('webpack-merge');

const path = require('path');

const webpack = require('webpack');

//const config = require('../../config/vue/production/app');
//const config = require('config/vue/configurator');

//console.log(config)

module.exports = merge(base, {

    mode: 'production',

    module: {
        rules: [

            // ...add your rules here!

        ]
    },

    plugins: [

        new webpack.DefinePlugin({
            'process': {
                //env: config
            }
        }),

    ]

});
