const base = require('./webpack.base');
const merge = require ('webpack-merge');

module.exports = merge(base, {

    mode: 'development',

    module: {
        rules: [

            // ...add your rules here!

        ]
    },

    plugins: [

        // ...add your plugins here!

    ],

    watch: true

});
