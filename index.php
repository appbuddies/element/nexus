<?php

require __DIR__ . '/bootstrap/bootstrap.php';

try {

    $app->run();

} catch (Throwable $e) {

    // ...do something

}