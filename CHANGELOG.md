# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).


## [2.0.0] - 2020.01.11
#### Added
- Vue.js with store is now fully integrated
- Phinx database migrations is now implementet into Element console
- Element now uses Laravels paginator for view pagination
- Swagger is now implemented, to describe all API endpoints

#### Changed
- Demo frontpage has got redesigned, so it's more stylish and modern
- Backend dashboard completely redesigned, into whats now called "Nexus admin"

#### Fixed
n/a

#### Removed

- Legacy vendors that were no longer used
- Deprecated "Basket + Braintree" code finally removed

## [1.0.3] - 2019.08.21
#### Added

#### Changed

#### Fixed
- Fixed the post-root-install issue there was, with copying the config files

#### Removed

